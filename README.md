# GetRusty Shop

The Rust Shop System is a high-speed, modular e-commerce platform built entirely in Rust using axum and seaORM. It features a powerful addon system that allows you to create custom products and modify their prices, as well as a robust stock system and order placement functionality.
Getting Started

To get started with the Rust Shop System, simply look into the [localdev](https://gitlab.com/getrusty/localdev) repository.

The Rust Shop System is designed for easy deployment using the fly.io platform. Continuous integration and deployment are handled automatically, with staging and production environments available at [https://shop.staging.getrusty.net/](https://shop.staging.getrusty.net/) and [https://shop.production.getrusty.net/](https://shop.production.getrusty.net/) respectively.
Authentication

The Rust Shop System uses the OpenID Connect standard for authentication, with a self-written microservice handling the authentication process. This ensures secure and reliable access to the platform.

## Addons

One of the standout features of the Rust Shop System is its powerful addon system, which allows you to create custom products and modify their prices. This feature is easy to use and highly customizable, allowing you to tailor the platform to your specific needs.

## Stock System

The Rust Shop System features a robust stock system that ensures accurate inventory management and timely order fulfillment. This system is designed for speed and reliability, ensuring that your customers always have access to the products they need.

## Orders

The Rust Shop System also features an intuitive order placement system, allowing your customers to easily place orders and track their progress. This system is designed to be user-friendly and efficient, ensuring that your customers always have a positive experience.

## Categories

Although categories are not currently available in the Rust Shop System, they are a planned feature that will be added in the near future. This will allow you to organize your products more effectively and provide a better experience for your customers.

## Documentation

The Rust Shop System includes an auto-generated swagger documentation, which can be found for staging at https://shop.staging.getrusty.net/docs/ and for production at https://shop.production.getrusty.net/docs/. This documentation is comprehensive and user-friendly, providing all the information you need to get the most out of the platform.

## Conclusion

The Rust Shop System is a powerful and flexible e-commerce platform that is designed for speed and reliability. With its powerful addon system, robust stock management, and intuitive order placement system, it is the perfect choice for any online retailer.

## License

Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence"); \
You may not use this work except in compliance with the Licence. \
You may obtain a copy of the Licence at:

[https://joinup.ec.europa.eu/software/page/eupl](https://joinup.ec.europa.eu/software/page/eupl)

Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. \
See the Licence for the specific language governing permissions and limitations under the Licence.
