/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
use std::{
    net::{IpAddr, SocketAddr},
    str::FromStr,
    sync::Arc,
    task::{Context, Poll},
};

use axum::{
    extract::{ConnectInfo, Request},
    http::StatusCode,
    response::{IntoResponse, Response},
};
use futures_util::future::BoxFuture;
use governor::{clock::QuantaClock, state::keyed::HashMapStateStore, Quota, RateLimiter};
use rauthy_client::principal::PrincipalOidc;
use tower::{Layer, Service};
use tracing::error;

#[derive(Debug, Clone)]
pub(crate) struct RateLimitLayer {
    quota: Quota,
}

impl RateLimitLayer {
    pub(crate) const fn new(quota: Quota) -> Self {
        Self { quota }
    }
}

impl<S> Layer<S> for RateLimitLayer {
    type Service = RateLimitMiddleware<S>;

    fn layer(&self, inner: S) -> Self::Service {
        RateLimitMiddleware {
            inner,
            rate_limits: Arc::new(RateLimiter::hashmap(self.quota)),
        }
    }
}

#[derive(Debug, Clone)]
pub(crate) struct RateLimitMiddleware<S> {
    inner: S,
    rate_limits: Arc<RateLimiter<IpAddr, HashMapStateStore<IpAddr>, QuantaClock>>,
}

impl<S> Service<Request> for RateLimitMiddleware<S>
where
    S: Service<Request, Response = Response> + Send + 'static,
    S::Future: Send + 'static,
{
    type Response = S::Response;
    type Error = S::Error;
    type Future = BoxFuture<'static, Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx)
    }

    fn call(&mut self, request: Request) -> Self::Future {
        let ip = if let Some(ip) = request.headers().get("X-Real-IP") {
            let ip = match ip.to_str() {
                Ok(ip) => ip,
                Err(error) => {
                    error!("unable to parse requets header 'X-Real-IP' to str, received error: {error:#?}");
                    return Box::pin(async {
                        Ok(StatusCode::INTERNAL_SERVER_ERROR.into_response())
                    });
                }
            };
            match IpAddr::from_str(ip) {
                Ok(ip) => ip,
                Err(error) => {
                    error!("unable to parse ip address from str, received error: {error:#?}");
                    return Box::pin(async {
                        Ok(StatusCode::INTERNAL_SERVER_ERROR.into_response())
                    });
                }
            }
        } else if let Some(ip) = request.extensions().get::<ConnectInfo<SocketAddr>>() {
            ip.0.ip()
        } else {
            error!("there is not X-Real-Ip header and no ConnectInfo<SocketAddr>, unable to get a real client ip");
            return Box::pin(async move { Ok(StatusCode::INTERNAL_SERVER_ERROR.into_response()) });
        };

        let rate_limit = match request.extensions().get::<PrincipalOidc>() {
            Some(principal) => {
                if principal.is_admin {
                    Ok(())
                } else {
                    self.rate_limits.check_key(&ip)
                }
            }
            None => self.rate_limits.check_key(&ip),
        };
        // FIXME
        // if let Err(not_until) = rate_limit {
        //     let not_until_seconds = not_until.wait_time_from(QuantaClock::default().now());
        //     let not_until_unix_epoch = (SystemTime::now() + not_until_seconds)
        //         .duration_since(SystemTime::UNIX_EPOCH)
        //         .unwrap_or_default()
        //         .as_secs();
        //     return Box::pin(async move {
        //         Ok((
        //             StatusCode::TOO_MANY_REQUESTS,
        //             Json(json!({
        //                 "error": "too many requests, please wait until the 'not_until' epoch time has reached",
        //                 "not_until": not_until_unix_epoch
        //             })),
        //         )
        //             .into_response())
        //     });
        // }

        let future = self.inner.call(request);

        Box::pin(future)
    }
}
