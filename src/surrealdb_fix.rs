/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use surrealdb::{error::Db, Error, Response};

pub(crate) trait CheckSurrealTransaction {
    fn check_transaction(self) -> Result<Response, Error>;
}

// A small warkaround for the broken error messages.
// See: https://github.com/Odonno/surrealdb-migrations/commit/698ba25bbdf827267d9c27b660c1f54699a98bd4
impl CheckSurrealTransaction for surrealdb::Response {
    fn check_transaction(mut self) -> Result<Response, Error> {
        let errors = self.take_errors();
        if errors.is_empty() {
            return Ok(self);
        }

        Err(errors
            .into_iter()
            .map(|(_, value)| value)
            .filter(|error| {
                error.to_string() != Error::Db(Db::QueryNotExecuted).to_string()
                    && error.to_string() != Error::Db(Db::TxFailure).to_string()
            })
            .next()
            .unwrap_or(Error::Db(Db::QueryNotExecuted)))
    }
}
