/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use std::fmt::Display;

use axum::{
    http::StatusCode,
    response::{IntoResponse, Response},
    Json,
};
use snafu::{prelude::*, Location};
use tracing::{error, info, warn};

#[allow(clippy::upper_case_acronyms)]
#[derive(Debug, Snafu)]
#[snafu(visibility(pub(crate)))]
pub(crate) enum ApiError {
    // Internal Libraries
    Pagination {
        source: pagination::error::PaginationError,
    },

    #[snafu(display("the parent id of the category would cause a circular dependency"))]
    CategoryCircularParent,

    #[snafu(display("received unexpected stripe error: `{source}`"))]
    Stripe {
        source: stripe::StripeError,
        #[snafu(implicit)]
        location: Location,
    },
    #[snafu(display("received unexpected surrealdb error: `{source}`"))]
    Surreal {
        source: surrealdb::Error,
        #[snafu(implicit)]
        location: Location,
    },
    #[snafu(display("Unable to parse to Uuid: `{source}`"))]
    Uuid {
        source: uuid::Error,
        #[snafu(implicit)]
        location: Location,
    },

    // Custom Errors
    #[snafu(transparent)]
    AllreadyExists {
        source: AllreadyExistsError,
        #[snafu(implicit)]
        location: Location,
    },
    #[snafu(transparent)]
    CategoryInUse {
        source: CategoryInUse,
        #[snafu(implicit)]
        location: Location,
    },
    #[snafu(transparent)]
    NotFound {
        source: NotFoundError,
        #[snafu(implicit)]
        location: Location,
    },
    #[snafu(transparent)]
    OrderError {
        source: OrderError,
        #[snafu(implicit)]
        location: Location,
    },

    // Other
    Other {
        source: anyhow::Error,
        #[snafu(implicit)]
        location: Location,
    },
}

impl From<pagination::error::PaginationError> for ApiError {
    fn from(value: pagination::error::PaginationError) -> Self {
        Self::Pagination { source: value }
    }
}

#[derive(Debug, Snafu)]
#[snafu(module)]
#[snafu(visibility(pub(crate)))]
pub(crate) enum AllreadyExistsError {
    #[snafu(display("attribute allready exists"))]
    Attribute,
    #[snafu(display("category allready exists"))]
    Category,
    #[snafu(display("media allready exists"))]
    Media,
    #[snafu(display("product allready exists"))]
    Product,
    #[snafu(display("order allready exists"))]
    _Order,
}

#[derive(Debug, Snafu)]
#[snafu(module)]
#[snafu(visibility(pub(crate)))]
pub(crate) enum CategoryInUse {
    #[snafu(display("category is in use by another category as parent"))]
    Category,
    #[snafu(display("category is in use by a product"))]
    Product,
}

#[derive(Debug, Snafu)]
#[snafu(module)]
#[snafu(visibility(pub(crate)))]
pub(crate) enum NotFoundError {
    #[snafu(display("attribute not found"))]
    Attribute,
    #[snafu(display("category not found"))]
    Category,
    #[snafu(display("media not found"))]
    Media,
    #[snafu(display("product not found"))]
    Product,
    #[snafu(display("product attribute option not found"))]
    ProductAttributeOption,
    #[snafu(display("product_variant not found"))]
    ProductVariant,
    #[snafu(display("order not found"))]
    Order,
}

#[derive(Debug, Snafu)]
#[snafu(module)]
#[snafu(visibility(pub(crate)))]
pub(crate) enum OrderError {
    #[snafu(display("unable to parse to stripe::CheckoutSessionId"))]
    CastCheckoutSessionId,
    #[snafu(display("unable to parse to stripe::CustomerId"))]
    CastCustomerId,
    #[snafu(display("unable to parse data from stripe to i32"))]
    CastStripeDataToI32,
    #[snafu(display("unable to parse data from stripe to Uuid"))]
    CastStripeDataToUuid,
    #[snafu(display("not enough items in stock, reduce maximum amount"))]
    NotEnoughInStock,
    #[snafu(display("product variant is out of stock"))]
    OutOfStock,
    #[snafu(display("the order is already valiated"))]
    OrderAlreadyValidated,
    #[snafu(display("order not successfully created"))]
    OrderNotSuccessfullyCreated,
    #[snafu(display("stripe checkout is not sending neough information"))]
    StripeCheckoutMissingData,
    #[snafu(display("stripe validation is not sending neough information"))]
    StripeValidationMissingData,
}

fn response(status: StatusCode, message: &'_ str) -> Response {
    let body = Json(serde_json::json!({
        "error": message,
    }));

    (status, body).into_response()
}

fn error(status: StatusCode, error: String) -> Response {
    response(status, error.as_str())
}

#[allow(clippy::match_same_arms)]
impl IntoResponse for ApiError {
    #[track_caller]
    fn into_response(self) -> Response {
        let error_message = self.to_string();
        let status_code = match self {
            // Internal Libraries
            Self::Pagination { source: error } => bad_request(error),
            Self::CategoryCircularParent => bad_request(self),
            // // External Libraries
            Self::Stripe { source, location } => {
                internal_server_error_with_location(source, location)
            }
            Self::Surreal { source, location } => {
                internal_server_error_with_location(source, location)
            }
            Self::Uuid { source, location } => {
                internal_server_error_with_location(source, location)
            }
            // Custom Errors
            Self::AllreadyExists { source, location } => conflict_with_location(source, location),
            Self::CategoryInUse { source, location } => bad_request_with_location(source, location),
            Self::NotFound { source, location } => not_found_error_with_location(source, location),
            Self::OrderError { source, location } => match source {
                OrderError::CastStripeDataToI32
                | OrderError::CastStripeDataToUuid
                | OrderError::StripeCheckoutMissingData
                | OrderError::StripeValidationMissingData
                | OrderError::OrderNotSuccessfullyCreated => {
                    internal_server_error_with_location(source, location)
                }
                source => bad_request_with_location(source, location),
            },
            // Other
            Self::Other { source, location } => not_implemented_with_location(source, location),
        };
        error(status_code, error_message)
    }
}

fn bad_request(source: impl Display) -> StatusCode {
    warn!(http.status = "BAD_REQUEST", "{source}");

    StatusCode::BAD_REQUEST
}

fn bad_request_with_location(source: impl Display, location: Location) -> StatusCode {
    warn!(
        http.status = "BAD_REQUEST",
        location.file = location.file,
        location.line = location.line,
        location.column = location.column,
        "{source}"
    );

    StatusCode::BAD_REQUEST
}

fn conflict_with_location(source: impl Display, location: Location) -> StatusCode {
    warn!(
        http.status = "CONFLICT",
        location.file = location.file,
        location.line = location.line,
        location.column = location.column,
        "{source}"
    );

    StatusCode::CONFLICT
}

fn internal_server_error_with_location(source: impl Display, location: Location) -> StatusCode {
    error!(
        http.status = "INTERNAL_SERVER_ERROR",
        location.file = location.file,
        location.line = location.line,
        location.column = location.column,
        "{source}"
    );

    StatusCode::INTERNAL_SERVER_ERROR
}

fn not_found_error_with_location(source: impl Display, location: Location) -> StatusCode {
    info!(
        http.status = "NOT_FOUND",
        location.file = location.file,
        location.line = location.line,
        location.column = location.column,
        "{source}"
    );

    StatusCode::NOT_FOUND
}

fn not_implemented_with_location(source: impl Display, location: Location) -> StatusCode {
    info!(
        http.status = "NOT_IMPLEMENTED",
        location.file = location.file,
        location.line = location.line,
        location.column = location.column,
        "{source}"
    );

    StatusCode::NOT_IMPLEMENTED
}
