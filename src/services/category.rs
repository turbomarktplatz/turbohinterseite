/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use async_trait::async_trait;
use model::{Category, CategoryId, NewCategory, Table, UpdateCategory};
use pagination::prelude::{Pagination, PaginationQuery};
use snafu::ResultExt;
use tracing::instrument;

use crate::{
    controllers::categories::ListAllCategoriesQuery,
    error::{ApiError, SurrealSnafu},
    ApiResult, AppData,
};

use super::CrudService;

#[async_trait]
pub(crate) trait CategoryService {
    async fn create_category(&self, payload: NewCategory) -> ApiResult<Category>;
    async fn find_category_in_page(
        &self,
        pagination: PaginationQuery,
        list_all_categories: &ListAllCategoriesQuery,
    ) -> ApiResult<Pagination<Category>>;
    async fn find_category_by_id(&self, category_id: CategoryId) -> ApiResult<Option<Category>>;
    async fn find_all_categories_by_parent_id(
        &self,
        parent_id: CategoryId,
    ) -> ApiResult<Vec<Category>>;
    async fn find_all_categories_recursive(
        &self,
        search_category_id: Option<CategoryId>,
    ) -> ApiResult<Vec<Category>>;
    async fn update_category(
        &self,
        category_id: CategoryId,
        payload: UpdateCategory,
    ) -> ApiResult<Option<Category>>;
    async fn delete_category(&self, category_id: CategoryId) -> ApiResult<()>;
}

#[async_trait]
impl CategoryService for AppData {
    #[instrument(skip(self))]
    async fn create_category(&self, payload: NewCategory) -> ApiResult<Category> {
        CrudService::<Category>::create(self, &payload).await
    }

    #[instrument(skip(self))]
    async fn find_category_in_page(
        &self,
        pagination: PaginationQuery,
        list_all_categories: &ListAllCategoriesQuery,
    ) -> ApiResult<Pagination<Category>> {
        let response = self
            .surreal
            .query(
                "
                    SELECT *
                    FROM type::table($table)
                    WHERE parent = $filter.parent_id
                    LIMIT $limit
                    START $start
                ",
            )
            .query("SELECT count() FROM type::table($table) GROUP ALL")
            .bind(("table", Category::TABLE))
            .bind(("limit", pagination.size))
            .bind(("start", (pagination.page - 1) * pagination.size))
            .bind(("filter", list_all_categories))
            .await
            .context(SurrealSnafu)?;

        (pagination, response).try_into().context(SurrealSnafu)
    }

    #[instrument(skip(self))]
    async fn find_category_by_id(&self, category_id: CategoryId) -> ApiResult<Option<Category>> {
        CrudService::<Category>::find_by_id(self, category_id).await
    }

    #[instrument(skip(self))]
    async fn find_all_categories_by_parent_id(
        &self,
        parent_id: CategoryId,
    ) -> ApiResult<Vec<Category>> {
        self.surreal
            .query("SELECT * FROM type::table($table) WHERE parent = $parent")
            .bind(("table", Category::TABLE))
            .bind(("parent", parent_id))
            .await
            .context(SurrealSnafu)?
            .take(0)
            .context(SurrealSnafu)
    }

    #[instrument(skip(self))]
    async fn find_all_categories_recursive(
        &self,
        mut search_category_id: Option<CategoryId>,
    ) -> ApiResult<Vec<Category>> {
        let mut categories = Vec::<Category>::new();
        while let Some(category_id) = search_category_id {
            let Some(category) = self.find_category_by_id(category_id).await? else {
                break;
            };
            categories.push(category.clone());
            search_category_id = category.parent;
        }

        Ok(categories.into_iter().rev().collect())
    }

    #[instrument(skip(self))]
    async fn update_category(
        &self,
        category_id: CategoryId,
        payload: UpdateCategory,
    ) -> ApiResult<Option<Category>> {
        if let Some(Some(parent_id)) = payload.parent.clone() {
            check_circular_dependencies(self, category_id.clone(), parent_id).await?;
        }

        CrudService::<Category>::update(self, category_id, &payload).await
    }

    #[instrument(skip(self))]
    async fn delete_category(&self, category_id: CategoryId) -> ApiResult<()> {
        CrudService::<Category>::delete(self, category_id).await
    }
}

#[instrument(skip(app_data))]
async fn check_circular_dependencies(
    app_data: &AppData,
    category_id: CategoryId,
    parent_id: CategoryId,
) -> ApiResult<()> {
    let mut check = parent_id.clone();

    if category_id == parent_id {
        return Err(ApiError::CategoryCircularParent);
    }

    while let Some(category) = app_data.find_category_by_id(check).await? {
        if category.id == category_id {
            return Err(ApiError::CategoryCircularParent);
        }
        if let Some(parent_id) = category.parent {
            check = parent_id;
            continue;
        }
        break;
    }

    Ok(())
}
