/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
use std::{
    collections::{HashMap, HashSet},
    str::FromStr,
};

use async_trait::async_trait;
use model::{
    Checkout, CheckoutGuest, Customer, CustomerId, NewCheckout, NewCustomerCheckout, NewOrderItem,
    Order, OrderId, OrderStatus, Product, StripeId, Table, UpdateOrder, ValidateCheckout,
};
use pagination::prelude::{Pagination, PaginationQuery};
use snafu::ResultExt;
use stripe::{
    CheckoutSession, CheckoutSessionBillingAddressCollection, CheckoutSessionId,
    CheckoutSessionItemId, CheckoutSessionMode, CheckoutSessionPaymentStatus,
    CreateCheckoutSession, CreateCheckoutSessionAutomaticTax, CreateCheckoutSessionCustomerUpdate,
    CreateCheckoutSessionCustomerUpdateAddress, CreateCheckoutSessionCustomerUpdateName,
    CreateCheckoutSessionCustomerUpdateShipping, CreateCheckoutSessionLineItems,
    CreateCheckoutSessionLineItemsPriceData, CreateCheckoutSessionLineItemsPriceDataProductData,
    CreateCheckoutSessionLineItemsPriceDataTaxBehavior, CreateCheckoutSessionPaymentMethodTypes,
    CreateCheckoutSessionShippingAddressCollection,
    CreateCheckoutSessionShippingAddressCollectionAllowedCountries,
    CreateCheckoutSessionShippingOptions, CreateCheckoutSessionShippingOptionsShippingRateData,
    CreateCheckoutSessionShippingOptionsShippingRateDataFixedAmount,
    CreateCheckoutSessionShippingOptionsShippingRateDataTaxBehavior, CreateCustomer, Currency,
    CustomerSearchParams, Expandable,
};
use surrealdb::sql::{
    statements::{BeginStatement, CommitStatement},
    Thing,
};
use tracing::instrument;

use crate::{
    error::{OrderError, StripeSnafu, SurrealSnafu},
    services::CrudService,
    surrealdb_fix::CheckSurrealTransaction,
    ApiResult, AppData,
};

use super::ProductService;

#[async_trait]
pub(crate) trait OrderService {
    async fn checkout_for_user(
        &self,
        payload: NewCustomerCheckout,
        email: String,
        given_name: String,
        family_name: String,
        customer_id: CustomerId,
    ) -> ApiResult<Checkout>;
    async fn checkout_as_guest(&self, payload: NewCheckout) -> ApiResult<CheckoutGuest>;
    async fn validate_checkout(&self, payload: ValidateCheckout) -> ApiResult<Order>;
    async fn find_checkout_in_page(
        &self,
        pagination: PaginationQuery,
    ) -> ApiResult<Pagination<Checkout>>;
    async fn find_checkout_by_customer_in_page(
        &self,
        user_id: String,
        pagination: PaginationQuery,
    ) -> ApiResult<Pagination<Checkout>>;
    async fn find_order_in_page(&self, pagination: PaginationQuery)
        -> ApiResult<Pagination<Order>>;
    async fn find_order_by_customer_in_page(
        &self,
        user_id: CustomerId,
        pagination: PaginationQuery,
    ) -> ApiResult<Pagination<Order>>;
    async fn find_order_by_id(&self, id: OrderId) -> ApiResult<Option<Order>>;
    async fn find_order_by_id_and_customer(
        &self,
        id: OrderId,
        customer_id: CustomerId,
    ) -> ApiResult<Option<Order>>;
    async fn update_order(&self, id: OrderId, payload: UpdateOrder) -> ApiResult<Option<Order>>;
}

#[async_trait]
impl OrderService for AppData {
    #[instrument(skip(self))]
    async fn checkout_for_user(
        &self,
        payload: NewCustomerCheckout,
        email: String,
        given_name: String,
        family_name: String,
        customer_id: CustomerId,
    ) -> ApiResult<Checkout> {
        let total_items = fetch_product(self, payload.items.clone()).await?;
        let shipping_fee = calculate_shipping_fee(self, &total_items);

        let customer = match self.find_by_id(customer_id.clone()).await? {
            Some(customer) => customer,
            None => {
                let customer: Option<Customer> = self
                    .surreal
                    .query(
                        "
                        fn::create_or_get('customer', {
                            id: $user_id,
                            email: $email,
                            first_name: $first_name,
                            last_name: $last_name
                        });
                        ",
                    )
                    .bind(("user_id", customer_id))
                    .bind(("email", email))
                    .bind(("first_name", given_name))
                    .bind(("last_name", family_name))
                    .await
                    .context(SurrealSnafu)?
                    .take(0)
                    .context(SurrealSnafu)?;
                let customer = customer.unwrap();

                let stripe_customer = stripe::Customer::search(
                    &self.stripe,
                    CustomerSearchParams {
                        query: format!("metadata['user_id']:'{}'", customer.id.to_string()),
                        limit: Some(1),
                        page: None,
                        expand: &[],
                    },
                )
                .await
                .context(StripeSnafu)?
                .data
                .pop();

                let stripe_id = if let Some(stripe_customer) = stripe_customer {
                    stripe_customer.id
                } else {
                    stripe::Customer::create(
                        &self.stripe,
                        CreateCustomer {
                            name: Some(
                                format!("{} {}", customer.first_name, customer.last_name).as_str(),
                            ),
                            email: Some(customer.email.as_str()),
                            metadata: Some(HashMap::from([(
                                "user_id".to_string(),
                                customer.id.to_string(),
                            )])),
                            ..Default::default()
                        },
                    )
                    .await
                    .context(StripeSnafu)?
                    .id
                };

                let customer: Option<Customer> = self
                    .surreal
                    .query("UPDATE ONLY $customer SET stripe_id = $stripe_id;")
                    .bind(("customer", Into::<Thing>::into(customer.id)))
                    .bind(("stripe_id", stripe_id))
                    .await
                    .context(SurrealSnafu)?
                    .take(0)
                    .context(SurrealSnafu)?;
                let customer = customer.unwrap();

                customer
            }
        };

        let checkout_id = ulid::Ulid::new().to_string();

        let checkout_url = stripe_checkout(
            self,
            &total_items,
            shipping_fee,
            customer.stripe_id,
            Some(checkout_id.as_str()),
        )
        .await?;

        let checkout: Option<Checkout> = self
            .surreal
            .query(BeginStatement)
            .query(
                "
                LET $checkout = CREATE ONLY checkout
                    CONTENT {
                        id: $checkout_id,
                        customer: $customer,
                        checkout_url: $checkout_url,
                    };
                ",
            )
            .query(
                "
                FOR $item in $items {
                    RELATE $checkout->order_item->(type::thing('product', $item[1].id))
                    SET
                        price = $item[1].price,
                        product_name = $item[1].name,
                        quantity = $item[0].quantity
                    ;
                };
                ",
            )
            .query("SELECT *,->order_item.* as items FROM $checkout;")
            .query(CommitStatement)
            .bind(("customer", Into::<Thing>::into(customer.id)))
            .bind(("checkout_id", checkout_id))
            .bind(("checkout_url", checkout_url))
            .bind(("items", total_items))
            .await
            .context(SurrealSnafu)?
            .check_transaction()
            .context(SurrealSnafu)?
            .take(2)
            .context(SurrealSnafu)?;
        let checkout = checkout.unwrap();

        Ok(checkout)
    }

    #[instrument(skip(self))]
    async fn checkout_as_guest(&self, payload: NewCheckout) -> ApiResult<CheckoutGuest> {
        let total_items = fetch_product(self, payload.items.clone()).await?;
        let shipping_fee = calculate_shipping_fee(self, &total_items);

        let checkout_url = stripe_checkout(self, &total_items, shipping_fee, None, None).await?;

        Ok(CheckoutGuest { checkout_url })
    }

    #[instrument(skip(self))]
    async fn validate_checkout(&self, payload: ValidateCheckout) -> ApiResult<Order> {
        let order: Option<Order> = self
            .surreal
            .query(
                "SELECT *,->order_item.* as items FROM ONLY order WHERE reference_id = $reference_id LIMIT 1;",
            )
            .bind(("reference_id", payload.validation_key.clone()))
            .await
            .context(SurrealSnafu)?
            .take(0)
            .context(SurrealSnafu)?;

        if let Some(order) = order {
            return Ok(order);
        }

        let Ok(checkout_session_id) = CheckoutSessionId::from_str(&payload.validation_key) else {
            return Err(OrderError::CastCheckoutSessionId.into());
        };

        let checkout = CheckoutSession::retrieve(
            &self.stripe,
            &checkout_session_id,
            &[
                "line_items",
                "line_items.data.price.product",
                "total_details.breakdown.discounts",
            ],
        )
        .await
        .context(StripeSnafu)?;

        if checkout.status != Some(stripe::CheckoutSessionStatus::Complete) {
            return Err(OrderError::StripeValidationMissingData.into());
        }

        let (Some(total_details), Some(Ok(amount_total)), Some(Ok(shipping_fee))) = (
            checkout.total_details,
            checkout.amount_total.map(i32::try_from),
            checkout
                .shipping_cost
                .map(|shipping_cost| i32::try_from(shipping_cost.amount_total)),
        ) else {
            return Err(OrderError::StripeValidationMissingData.into());
        };

        let (Ok(amount_discount), Ok(amount_tax)) = (
            i32::try_from(total_details.amount_discount),
            i32::try_from(total_details.amount_tax),
        ) else {
            return Err(OrderError::StripeValidationMissingData.into());
        };

        let status = match checkout.payment_status {
            CheckoutSessionPaymentStatus::NoPaymentRequired
            | CheckoutSessionPaymentStatus::Paid => OrderStatus::Processing,
            CheckoutSessionPaymentStatus::Unpaid => OrderStatus::Pending,
        };

        let Some(shipping_details) = checkout.shipping_details else {
            return Err(OrderError::StripeValidationMissingData.into());
        };

        let shipping_name = shipping_details.name;
        let Some(shipping_address) = shipping_details.address else {
            return Err(OrderError::StripeValidationMissingData.into());
        };

        let shipping_address_line2 = shipping_address.line2;
        let (
            Some(shipping_address_line1),
            Some(shipping_city),
            Some(shipping_postal_code),
            Some(shipping_address_country),
        ) = (
            shipping_address.line1,
            shipping_address.city,
            shipping_address.postal_code,
            shipping_address.country,
        )
        else {
            return Err(OrderError::StripeValidationMissingData.into());
        };

        let shipping_state = shipping_address.state;

        let customer: Option<Customer> = if let Some(customer) = checkout.customer {
            self.surreal
                .query("SELECT * FROM ONLY customer WHERE stripe_id = $stripe_id LIMIT 1;")
                .bind(("stripe_id", customer.id()))
                .await
                .context(SurrealSnafu)?
                .take(0)
                .context(SurrealSnafu)?
        } else {
            None
        };

        let mut items: Vec<(String, String, i32, i32, i32, i32, i32)> = Vec::new();
        let mut line_items = checkout.line_items.unwrap();
        loop {
            for item in &line_items.data {
                let Some(metadata) = item
                    .clone()
                    .price
                    .and_then(|price| price.product)
                    .and_then(Expandable::into_object)
                    .and_then(|object| object.metadata)
                else {
                    return Err(OrderError::StripeValidationMissingData.into());
                };

                let (
                    Some(product_id),
                    Some(Ok(quantity)),
                    Some(Ok(price)),
                    Ok(amount_discount),
                    Ok(amount_tax),
                    Ok(amount_total),
                ) = (
                    metadata.get("product_id"),
                    item.quantity.map(i32::try_from),
                    item.price
                        .clone()
                        .and_then(|price| price.unit_amount)
                        .map(i32::try_from),
                    i32::try_from(item.amount_discount),
                    i32::try_from(item.amount_tax),
                    i32::try_from(item.amount_total),
                )
                else {
                    return Err(OrderError::StripeValidationMissingData.into());
                };

                items.push((
                    product_id.to_owned(),
                    item.description.to_owned(),
                    quantity,
                    price,
                    amount_discount,
                    amount_tax,
                    amount_total,
                ));
            }

            log::debug!("has_more: {}", line_items.has_more);

            if !line_items.has_more {
                break;
            }

            let data = line_items.data.clone();
            log::debug!("has_more: {:?}", data.last());
            let Some(line_item) = data.last() else {
                break;
            };

            #[derive(Clone, Debug, serde::Serialize, Default)]
            pub struct RetrieveCheckoutSessionLineItems<'a> {
                pub starting_after: Option<CheckoutSessionItemId>,
                pub expand: &'a [&'a str],
            }

            line_items = self
                .stripe
                .get_query(
                    &format!("/checkout/sessions/{}/line_items", checkout_session_id),
                    &RetrieveCheckoutSessionLineItems {
                        starting_after: Some(line_item.id.clone()),
                        expand: &["data.price.product"],
                    },
                )
                .await
                .context(StripeSnafu)?;
        }

        let order: Option<Order> = self
            .surreal
            .query(BeginStatement)
            .query(
                "
                LET $order= CREATE ONLY order
                    CONTENT {
                        reference_id: $reference_id,
                        customer: $customer,
                        shipping_name: $shipping_name,
                        shipping_line1: $shipping_line1,
                        shipping_line2: $shipping_line2,
                        shipping_city: $shipping_city,
                        shipping_state: $shipping_state,
                        shipping_zip_code: $shipping_zip_code,
                        shipping_country: $shipping_country,
                        shipping_fee: $shipping_fee,
                        amount_discount: $amount_discount,
                        amount_tax: $amount_tax,
                        amount_total: $amount_total,
                        status: $status,
                    };
                ",
            )
            .query(
                "
                FOR $item in $items {
                    RELATE $order->order_item->(type::thing('product', $item[0]))
                    SET
                        product_name = $item[1],
                        quantity = $item[2],
                        price = $item[3],
                        amount_discount = $item[4],
                        amount_tax = $item[5],
                        amount_total = $item[6] 
                    ;
                };
                ",
            )
            .query("SELECT *,->order_item.* as items FROM $order;")
            .query(CommitStatement)
            .bind((
                "customer",
                customer.map(|customer| Into::<Thing>::into(customer.id)),
            ))
            .bind(("reference_id", payload.validation_key))
            .bind(("shipping_name", shipping_name))
            .bind(("shipping_line1", shipping_address_line1))
            .bind(("shipping_line2", shipping_address_line2))
            .bind(("shipping_city", shipping_city))
            .bind(("shipping_state", shipping_state))
            .bind(("shipping_zip_code", shipping_postal_code))
            .bind(("shipping_country", shipping_address_country))
            .bind(("shipping_fee", shipping_fee))
            .bind(("amount_discount", amount_discount))
            .bind(("amount_tax", amount_tax))
            .bind(("amount_total", amount_total))
            .bind(("status", status))
            .bind(("items", items))
            .await
            .context(SurrealSnafu)?
            .check_transaction()
            .context(SurrealSnafu)?
            .take(2)
            .context(SurrealSnafu)?;
        let order = order.unwrap();

        Ok(order)
    }

    #[instrument(skip(self))]
    async fn find_checkout_in_page(
        &self,
        pagination: PaginationQuery,
    ) -> ApiResult<Pagination<Checkout>> {
        let response = self
            .surreal
            .query(format!(
                "
                    SELECT *,->order_item.* as items
                    FROM type::table($table)
                    ORDER BY created {order_dir}
                    LIMIT $limit
                    START $start
                ",
                order_dir = pagination.order_dir
            ))
            .query("SELECT count() FROM type::table($table) GROUP ALL")
            .bind(("table", Checkout::TABLE))
            .bind(("limit", pagination.size))
            .bind(("start", (pagination.page - 1) * pagination.size))
            .await
            .context(SurrealSnafu)?;

        (pagination, response).try_into().context(SurrealSnafu)
    }

    #[instrument(skip(self))]
    async fn find_checkout_by_customer_in_page(
        &self,
        user_id: String,
        pagination: PaginationQuery,
    ) -> ApiResult<Pagination<Checkout>> {
        let response = self
            .surreal
            .query(format!(
                "
                    SELECT *,->order_item.* as items
                    FROM type::table($table)
                    WHERE customer = type::thing('customer', $customer_id)
                    LIMIT $limit
                    START $start
                    ODER BY id ${order_dir}
                ",
                order_dir = pagination.order_dir
            ))
            .query("SELECT count() FROM type::table($table) GROUP ALL")
            .bind(("table", Checkout::TABLE))
            .bind(("customer_id", user_id))
            .bind(("limit", pagination.size))
            .bind(("start", (pagination.page - 1) * pagination.size))
            .await
            .context(SurrealSnafu)?;

        (pagination, response).try_into().context(SurrealSnafu)
    }

    #[instrument(skip(self))]
    async fn find_order_in_page(
        &self,
        pagination: PaginationQuery,
    ) -> ApiResult<Pagination<Order>> {
        let response = self
            .surreal
            .query(format!(
                "
                    SELECT *,->order_item.* as items
                    FROM type::table($table)
                    ORDER BY created {order_dir}
                    LIMIT $limit
                    START $start
                ",
                order_dir = pagination.order_dir
            ))
            .query("SELECT count() FROM type::table($table) GROUP ALL")
            .bind(("table", Order::TABLE))
            .bind(("limit", pagination.size))
            .bind(("start", (pagination.page - 1) * pagination.size))
            .await
            .context(SurrealSnafu)?;

        (pagination, response).try_into().context(SurrealSnafu)
    }

    #[instrument(skip(self))]
    async fn find_order_by_customer_in_page(
        &self,
        customer_id: CustomerId,
        pagination: PaginationQuery,
    ) -> ApiResult<Pagination<Order>> {
        let response = self
            .surreal
            .query(format!(
                "
                    SELECT *,->order_item.* as items
                    FROM type::table($table)
                    WHERE customer = $customer_id
                    ORDER BY created {order_dir}
                    LIMIT $limit
                    START $start
                ",
                order_dir = pagination.order_dir
            ))
            .query(
                "
                    SELECT count()
                    FROM type::table($table)
                    WHERE customer = $customer_id
                    GROUP ALL
                ",
            )
            .bind(("table", Order::TABLE))
            .bind(("customer_id", Into::<Thing>::into(customer_id)))
            .bind(("limit", pagination.size))
            .bind(("start", (pagination.page - 1) * pagination.size))
            .await
            .context(SurrealSnafu)?;

        (pagination, response).try_into().context(SurrealSnafu)
    }

    #[instrument(skip(self))]
    async fn find_order_by_id(&self, id: OrderId) -> ApiResult<Option<Order>> {
        self.surreal
            .query(
                "
                SELECT *,->order_item.* as items
                FROM ONLY type::thing($order_id)
                ",
            )
            .bind(("order_id", Into::<Thing>::into(id)))
            .await
            .context(SurrealSnafu)?
            .take(0)
            .context(SurrealSnafu)
    }

    #[instrument(skip(self))]
    async fn find_order_by_id_and_customer(
        &self,
        id: OrderId,
        customer_id: CustomerId,
    ) -> ApiResult<Option<Order>> {
        self.surreal
            .query(
                "
                SELECT *,->order_item.* as items
                FROM ONLY type::thing($order_id)
                WHERE customer = $customer_id LIMIT 1;
                ",
            )
            .bind(("order_id", id))
            .bind(("customer_id", Into::<Thing>::into(customer_id)))
            .await
            .context(SurrealSnafu)?
            .take(0)
            .context(SurrealSnafu)
    }

    #[instrument(skip(self))]
    async fn update_order(&self, id: OrderId, payload: UpdateOrder) -> ApiResult<Option<Order>> {
        self.surreal
            .query(
                "
                UPDATE ONLY type::thing($order_id)
                SET status = $payload.status
                RETURN *,->order_item.* as items
                ",
            )
            .bind(("order_id", id))
            .bind(("payload", payload))
            .await
            .context(SurrealSnafu)?
            .take(0)
            .context(SurrealSnafu)
    }
}

#[instrument(skip(app_data))]
async fn fetch_product(
    app_data: &AppData,
    items: HashSet<NewOrderItem>,
) -> ApiResult<Vec<(NewOrderItem, Product)>> {
    let mut total_items = Vec::new();
    for item in items {
        let Some(product) = app_data
            .find_product_by_id(item.product.clone())
            .await
            .unwrap()
        else {
            panic!("product not found")
        };

        // FIXME: Check stock
        // if !product.preorder {
        //     if product_variant.stock <= 0 {
        //         return Err(OrderError::OutOfStock.into());
        //     } else if item.quantity > product_variant.stock {
        //         return Err(OrderError::NotEnoughInStock.into());
        //     }
        // }

        total_items.push((item, product));
    }

    Ok(total_items)
}

#[instrument(skip(app_data))]
pub(crate) fn calculate_shipping_fee(
    app_data: &AppData,
    total_items: &[(NewOrderItem, Product)],
) -> i32 {
    let total_items_price = total_items
        .iter()
        .filter_map(|(item, product)| product.price.map(|price| price * item.quantity))
        .sum::<i32>();

    app_data
        .shipping_fees
        .clone()
        .into_iter()
        .filter(|shipping_fee| {
            shipping_fee.min <= total_items_price
                && (shipping_fee.max > total_items_price || shipping_fee.max <= 0)
        })
        .map(|shipping_fee| shipping_fee.price)
        .collect::<Vec<i32>>()
        .first()
        .copied()
        .unwrap_or(0)
}

#[allow(clippy::cast_lossless, clippy::cast_sign_loss)]
#[instrument(skip(app_data))]
async fn stripe_checkout(
    app_data: &AppData,
    total_items: &[(NewOrderItem, Product)],
    shipping_fee: i32,
    customer: Option<StripeId>,
    order_id: Option<&str>,
) -> ApiResult<String> {
    let checkout_session = CheckoutSession::create(
        &app_data.stripe,
        CreateCheckoutSession {
            mode: Some(CheckoutSessionMode::Payment),
            allow_promotion_codes: Some(true),
            client_reference_id: order_id,
            customer: customer.clone(),
            customer_update: customer.map(|_|
                CreateCheckoutSessionCustomerUpdate {
                    address: Some(CreateCheckoutSessionCustomerUpdateAddress::Auto),
                    name: Some(CreateCheckoutSessionCustomerUpdateName::Auto),
                    shipping: Some(CreateCheckoutSessionCustomerUpdateShipping::Auto),
                }
            ),
            billing_address_collection: Some(CheckoutSessionBillingAddressCollection::Auto),
            shipping_address_collection: Some(CreateCheckoutSessionShippingAddressCollection {
                allowed_countries: vec![
                    CreateCheckoutSessionShippingAddressCollectionAllowedCountries::De
                ],
            }),
            payment_method_types: Some(vec![
                CreateCheckoutSessionPaymentMethodTypes::Paypal,
                CreateCheckoutSessionPaymentMethodTypes::Card,
                CreateCheckoutSessionPaymentMethodTypes::Giropay,
            ]),
            line_items: Some(
                total_items
                    .iter()
                    .map(|(item, product)| {
                        let line_item = CreateCheckoutSessionLineItems {
                            quantity: Some(item.quantity as u64),
                            price_data: Some(CreateCheckoutSessionLineItemsPriceData {
                                currency: Currency::EUR,
                                product_data: Some(
                                    CreateCheckoutSessionLineItemsPriceDataProductData {
                                        name: product.name.clone(),
                                        metadata: Some(HashMap::from([("product_id".to_owned(), item.product.to_string())])),
                                        tax_code: Some(String::from("txcd_00000000")),
                                        ..Default::default()
                                    },
                                ),
                                tax_behavior: Some(CreateCheckoutSessionLineItemsPriceDataTaxBehavior::Inclusive),
                                unit_amount: product.price.map(Into::into),
                                ..Default::default()
                            }),
                            ..Default::default()
                        };

                        line_item
                    })
                    .collect::<Vec<_>>(),
            ),
            automatic_tax: Some(CreateCheckoutSessionAutomaticTax { enabled: true, liability: None }),
            shipping_options: Some(vec![CreateCheckoutSessionShippingOptions {
                shipping_rate_data: Some(CreateCheckoutSessionShippingOptionsShippingRateData {
                    display_name: "DHL".to_string(),
                    fixed_amount: Some(
                        CreateCheckoutSessionShippingOptionsShippingRateDataFixedAmount {
                            amount: shipping_fee as i64,
                            currency: Currency::EUR,
                            ..Default::default()
                        },
                    ),
                    tax_behavior: Some(
                        CreateCheckoutSessionShippingOptionsShippingRateDataTaxBehavior::Inclusive,
                    ),
                    tax_code: Some(String::from("txcd_00000000")),
                    type_: Some(stripe::CreateCheckoutSessionShippingOptionsShippingRateDataType::FixedAmount),
                    ..Default::default()
                }),
                ..Default::default()
            }]),
            cancel_url: Some(format!("{}/order/cancel", app_data.public_url.clone()).as_str()),
            success_url: Some(format!("{}/order/success?session_id={{CHECKOUT_SESSION_ID}}", app_data.public_url).as_str()),
            ..CreateCheckoutSession::new()
        },
    )
    .await
    .context(StripeSnafu)?;

    let Some(checkout_url) = checkout_session.url else {
        return Err(OrderError::StripeCheckoutMissingData.into());
    };

    Ok(checkout_url)
}
