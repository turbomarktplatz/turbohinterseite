/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use async_trait::async_trait;
use model::{Customer, CustomerId};
use pagination::prelude::{Pagination, PaginationQuery};
use tracing::instrument;

use crate::{ApiResult, AppData};

#[async_trait]
pub(crate) trait CustomerService {
    async fn find_customer_in_page(
        &self,
        pagination: PaginationQuery,
    ) -> ApiResult<Pagination<Customer>>;
    async fn find_customer_by_id(&self, id: CustomerId) -> ApiResult<Option<Customer>>;
    async fn delete_customer(&self, id: CustomerId) -> ApiResult<()>;
}

#[async_trait]
impl CustomerService for AppData {
    #[instrument(skip(self))]
    async fn find_customer_in_page(
        &self,
        pagination: PaginationQuery,
    ) -> ApiResult<Pagination<Customer>> {
        // let customers = CustomerQuery::find_in_page(&self.db_conn, pagination)
        //     .await?
        //     .into_type();
        // Ok(customers)
        unimplemented!()
    }

    #[instrument(skip(self))]
    async fn find_customer_by_id(&self, id: CustomerId) -> ApiResult<Option<Customer>> {
        // let customer = CustomerQuery::find_by_id(&self.db_conn, id)
        //     .await?
        //     .map(std::convert::Into::into);
        // Ok(customer)
        unimplemented!()
    }

    #[instrument(skip(self))]
    async fn delete_customer(&self, id: CustomerId) -> ApiResult<()> {
        // CustomerQuery::delete(&self.db_conn, id).await?;
        // Ok(())
        unimplemented!()
    }
}
