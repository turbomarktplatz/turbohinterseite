/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
mod category;
mod customer;
mod media;
mod order;
mod product;

pub(crate) use category::*;
pub(crate) use media::*;
pub(crate) use order::*;
pub(crate) use product::*;
use snafu::ResultExt;

use std::{fmt::Debug, hash::Hash};

use async_trait::async_trait;
use model::*;
use pagination::prelude::{Pagination, PaginationQuery};
use serde::{de::DeserializeOwned, Serialize};
use surrealdb::{sql::Thing, Surreal};
use tracing::instrument;

use crate::{error::SurrealSnafu, ApiResult, AppData};

pub(crate) trait GetSurreal {
    fn surrealdb(&self) -> Surreal<surrealdb::engine::any::Any>;
}

impl GetSurreal for AppData {
    fn surrealdb(&self) -> Surreal<surrealdb::engine::any::Any> {
        self.surreal.clone()
    }
}

#[async_trait]
pub(crate) trait CrudService<Model>: GetSurreal
where
    Model: DeserializeOwned + Debug + Table + Send + Sync + 'static,
{
    #[instrument(skip(self))]
    async fn create<NewModel>(&self, payload: &NewModel) -> ApiResult<Model>
    where
        NewModel: Serialize + Debug + Send + Sync,
    {
        let mut models = self
            .surrealdb()
            .create(Model::TABLE)
            .content(payload)
            .await
            .context(SurrealSnafu)?;

        // FIXME: Use error return instead of unwrap
        Ok(models.pop().unwrap())
    }

    #[instrument(skip(self))]
    async fn find_in_page(&self, pagination: PaginationQuery) -> ApiResult<Pagination<Model>> {
        let response = self
            .surrealdb()
            .query(
                "
                    SELECT *
                    FROM type::table($table)
                    LIMIT $limit
                    START $start
                ",
            )
            .query("SELECT count() FROM type::table($table) GROUP ALL")
            .bind(("table", Model::TABLE))
            .bind(("limit", pagination.size))
            .bind(("start", (pagination.page - 1) * pagination.size))
            .await
            .context(SurrealSnafu)?;

        (pagination, response).try_into().context(SurrealSnafu)
    }

    #[instrument(skip(self))]
    async fn find_by_id<ID>(&self, id: Id<Model, ID>) -> ApiResult<Option<Model>>
    where
        ID: Debug
            + PartialEq
            + Eq
            + Hash
            + DeserializeOwned
            + Serialize
            + Into<surrealdb::sql::Id>
            + Send,
    {
        self.surrealdb()
            .select(Into::<Thing>::into(id))
            .await
            .context(SurrealSnafu)
    }

    #[instrument(skip(self))]
    async fn update<UpdateModel, ID: DeserializeOwned>(
        &self,
        id: Id<Model, ID>,
        payload: &UpdateModel,
    ) -> ApiResult<Option<Model>>
    where
        UpdateModel: Serialize + Debug + Send + Sync,
        ID: Debug
            + PartialEq
            + Eq
            + Hash
            + DeserializeOwned
            + Serialize
            + Into<surrealdb::sql::Id>
            + Send,
    {
        self.surrealdb()
            .update(Into::<Thing>::into(id))
            .merge(payload)
            .await
            .context(SurrealSnafu)
    }

    #[instrument(skip(self))]
    async fn delete<ID>(&self, id: Id<Model, ID>) -> ApiResult<()>
    where
        ID: Debug
            + PartialEq
            + Eq
            + Hash
            + DeserializeOwned
            + Serialize
            + Into<surrealdb::sql::Id>
            + Send,
    {
        self.surrealdb()
            .query("DELETE type::thing($thing);")
            .bind(("thing", Into::<Thing>::into(id)))
            .await
            .context(SurrealSnafu)?
            .check()
            .context(SurrealSnafu)?;

        Ok(())
    }
}

impl CrudService<Category> for AppData {}
impl CrudService<Customer> for AppData {}
impl CrudService<Media> for AppData {}
impl CrudService<Order> for AppData {}
impl CrudService<OrderItem> for AppData {}
impl CrudService<Product> for AppData {}
