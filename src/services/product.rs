/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use async_trait::async_trait;
use model::{CategoryId, NewProduct, Product, ProductId, ProductSitemap, Table, UpdateProduct};
use pagination::prelude::{Pagination, PaginationQuery};
use snafu::prelude::*;
use surrealdb::sql::{
    statements::{BeginStatement, CommitStatement},
    Thing,
};
use tracing::instrument;

use crate::{
    controllers::products::ListAllProductsQuery, error::SurrealSnafu,
    surrealdb_fix::CheckSurrealTransaction, ApiResult, AppData,
};

#[async_trait]
pub(crate) trait ProductService {
    async fn create_product(&self, payload: NewProduct) -> ApiResult<Option<Product>>;
    async fn find_sitemap_products(&self) -> ApiResult<Vec<ProductSitemap>>;
    async fn find_product_in_page(
        &self,
        pagination: PaginationQuery,
        list_all_products: &ListAllProductsQuery,
    ) -> ApiResult<Pagination<Product>>;
    async fn find_bestseller_products(
        &self,
        pagination: PaginationQuery,
    ) -> ApiResult<Pagination<Product>>;
    async fn find_all_products_by_category_id(
        &self,
        category_id: CategoryId,
    ) -> ApiResult<Vec<Product>>;
    async fn find_all_product_ids(&self) -> ApiResult<Vec<ProductId>>;
    async fn find_product_by_id(&self, product_id: ProductId) -> ApiResult<Option<Product>>;
    async fn find_product_by_ids(&self, product_ids: Vec<ProductId>) -> ApiResult<Vec<Product>>;
    async fn check_product_exists(&self, product_id: ProductId) -> ApiResult<Option<bool>>;
    async fn update_product(
        &self,
        product_id: ProductId,
        payload: UpdateProduct,
    ) -> ApiResult<Option<Product>>;
    async fn delete_product(&self, product_id: ProductId) -> ApiResult<()>;
}

const NORMAL_SELECT: &str = r#"
    id, name, description, metadata, created, updated, price, is_orderable, preview_media.*,
    fn::fetch_category_parent(category) as category
"#;

const DETAILED_SELECT: &str = const_format::formatcp!(
    r#"
    {NORMAL_SELECT},
    (SELECT out,order FROM ->product_media ORDER BY order).out.* as medias,
    (
        SELECT *,(SELECT id,name FROM ->characterize->option) as options
        FROM ->specify->option<-characterize<-attribute
    ) AS attributes,
    (
        SELECT id,->specify->option.id as options
        FROM array::distinct(->specify->option<-characterize<-attribute->characterize->option<-specify<-product)
    ) AS associations,
    ->specify->option.id as options
"#
);

#[async_trait]
impl ProductService for AppData {
    #[instrument(skip(self))]
    async fn create_product(&self, payload: NewProduct) -> ApiResult<Option<Product>> {
        self.surreal
            .query(BeginStatement)
            .query(r#"LET $preview_media = fn::create_or_get_id("media", $data.preview_media);"#)
            .query(
                "
                LET $product = (
                    CREATE type::thing($table, $data.id)
                    SET
                        category = $data.category,
                        name = $data.name,
                        description = $data.description,
                        metadata = $data.metadata,
                        preview_media = $preview_media,
                        created = time::now(),
                        updated = time::now()
                );
                ",
            )
            .query(
                "
                FOR $media IN $data.medias {
                    fn::create_or_relate_product_media($data.id, $media);
                };
                ",
            )
            .query(
                "FOR $specify IN $data.specify {
                    fn::specify($data.id, $specify.attribute, $specify.option);
                };",
            )
            .query(format!("SELECT {DETAILED_SELECT} FROM $product;"))
            .query(CommitStatement)
            .bind(("table", Product::TABLE))
            .bind(("data", payload.clone()))
            .await
            .context(SurrealSnafu)?
            .check_transaction()
            .context(SurrealSnafu)?
            .take(4)
            .context(SurrealSnafu)
    }

    async fn find_sitemap_products(&self) -> ApiResult<Vec<ProductSitemap>> {
        self.surreal
            .query("select id,name from product;")
            .await
            .context(SurrealSnafu)?
            .check()
            .context(SurrealSnafu)?
            .take(0)
            .context(SurrealSnafu)
    }

    #[instrument(skip(self))]
    async fn find_product_in_page(
        &self,
        pagination: PaginationQuery,
        list_all_products: &ListAllProductsQuery,
    ) -> ApiResult<Pagination<Product>> {
        let response = self
            .surreal
            .query(format!(
                "
                    SELECT {NORMAL_SELECT}
                    FROM (
                        SELECT id
                        FROM IF $filter.category_id {{
                            type::thing('category', $filter.category_id)<-categorize<-product
                        }} ELSE {{
                            type::table($table)
                        }}
                        LIMIT $limit
                        START $start
                    ).id
                "
            ))
            .query("SELECT count() FROM type::table($table) GROUP ALL")
            .bind(("table", Product::TABLE))
            .bind(("limit", pagination.size))
            .bind(("start", (pagination.page - 1) * pagination.size))
            .bind(("filter", list_all_products))
            .await
            .context(SurrealSnafu)?;

        (pagination, response).try_into().context(SurrealSnafu)
    }

    async fn find_bestseller_products(
        &self,
        pagination: PaginationQuery,
    ) -> ApiResult<Pagination<Product>> {
        let response = self
            .surreal
            .query(format!(
                "
                    SELECT {NORMAL_SELECT} FROM (
                        SELECT count() AS amount, out AS product
                        FROM order_item
                        GROUP BY product
                        ORDER BY amount DESC
                        LIMIT $limit
                        START $start
                    ).product
                ",
            ))
            .query(
                "
                    [{count: count((
                        SELECT count() AS amount, out AS product
                        FROM order_item
                        GROUP BY product
                    ))}]
                ",
            )
            .bind(("table", Product::TABLE))
            .bind(("limit", pagination.size))
            .bind(("start", (pagination.page - 1) * pagination.size))
            .await
            .context(SurrealSnafu)?;

        (pagination, response).try_into().context(SurrealSnafu)
    }

    #[instrument(skip(self))]
    async fn find_all_products_by_category_id(
        &self,
        category_id: CategoryId,
    ) -> ApiResult<Vec<Product>> {
        self.surreal
            .query(format!(
                "
                SELECT {DETAILED_SELECT}
                FROM type::thing($category)<-categorize<-product
                "
            ))
            .bind(("table", Product::TABLE))
            .bind(("category", Into::<Thing>::into(category_id)))
            .await
            .context(SurrealSnafu)?
            .take(0)
            .context(SurrealSnafu)
    }

    #[instrument(skip(self))]
    async fn find_all_product_ids(&self) -> ApiResult<Vec<ProductId>> {
        self.surreal
            .query(format!("(SELECT id FROM type::table($table)).id;"))
            .bind(("table", Product::TABLE))
            .await
            .context(SurrealSnafu)?
            .take(0)
            .context(SurrealSnafu)
    }

    #[instrument(skip(self))]
    async fn find_product_by_id(&self, product_id: ProductId) -> ApiResult<Option<Product>> {
        let response = self
            .surreal
            .query(format!(
                "SELECT {DETAILED_SELECT} FROM ONLY type::thing($product);"
            ))
            .bind(("product", Into::<Thing>::into(product_id)))
            .await
            .context(SurrealSnafu)?
            .take(0)
            .context(SurrealSnafu)?;

        Ok(response)
    }

    #[instrument(skip(self))]
    async fn find_product_by_ids(&self, product_ids: Vec<ProductId>) -> ApiResult<Vec<Product>> {
        self.surreal
            .query(format!(
                "SELECT {DETAILED_SELECT} FROM type::thing($products);"
            ))
            .bind((
                "products",
                product_ids
                    .into_iter()
                    .map(|product_id| Into::<Thing>::into(product_id))
                    .collect::<Vec<_>>(),
            ))
            .await
            .context(SurrealSnafu)?
            .take(0)
            .context(SurrealSnafu)
    }

    #[instrument(skip(self))]
    async fn check_product_exists(&self, product_id: ProductId) -> ApiResult<Option<bool>> {
        self.surreal
            .query(format!(
                "SELECT count() FROM type::thing($table, $product);"
            ))
            .bind(("table", Product::TABLE))
            .bind(("product", Into::<Thing>::into(product_id)))
            .await
            .context(SurrealSnafu)?
            .take::<Option<bool>>(0)
            .context(SurrealSnafu)
    }

    #[instrument(skip(self))]
    #[track_caller]
    async fn update_product(
        &self,
        product_id: ProductId,
        payload: UpdateProduct,
    ) -> ApiResult<Option<Product>> {
        unimplemented!()
    }

    #[instrument(skip(self))]
    async fn delete_product(&self, product_id: ProductId) -> ApiResult<()> {
        unimplemented!()
    }
}
