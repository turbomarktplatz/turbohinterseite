/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use async_trait::async_trait;
use model::{Media, MediaId, NewMedia, UpdateMedia};
use pagination::prelude::{Pagination, PaginationQuery};
use tracing::instrument;

use crate::{ApiResult, AppData};

use super::CrudService;

#[async_trait]
pub(crate) trait MediaService {
    async fn create_media(&self, payload: NewMedia) -> ApiResult<Media>;
    async fn find_media_in_page(&self, pagination: PaginationQuery)
        -> ApiResult<Pagination<Media>>;
    async fn find_media_by_id(&self, id: MediaId) -> ApiResult<Option<Media>>;
    async fn update_media(&self, id: MediaId, payload: UpdateMedia) -> ApiResult<Option<Media>>;
    async fn delete_media(&self, id: MediaId) -> ApiResult<()>;
}

#[async_trait]
impl MediaService for AppData {
    #[instrument(skip(self))]
    async fn create_media(&self, payload: NewMedia) -> ApiResult<Media> {
        CrudService::<Media>::create(self, &payload).await
    }

    #[instrument(skip(self))]
    async fn find_media_in_page(
        &self,
        pagination: PaginationQuery,
    ) -> ApiResult<Pagination<Media>> {
        CrudService::<Media>::find_in_page(self, pagination).await
    }

    #[instrument(skip(self))]
    async fn find_media_by_id(&self, id: MediaId) -> ApiResult<Option<Media>> {
        CrudService::<Media>::find_by_id(self, id).await
    }

    #[instrument(skip(self))]
    async fn update_media(&self, id: MediaId, payload: UpdateMedia) -> ApiResult<Option<Media>> {
        CrudService::<Media>::update(self, id, &payload).await
    }

    #[instrument(skip(self))]
    async fn delete_media(&self, id: MediaId) -> ApiResult<()> {
        CrudService::<Media>::delete(self, id).await
    }
}
