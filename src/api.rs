/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
use model::*;
use utoipa::{
    openapi::security::{HttpAuthScheme, HttpBuilder, SecurityScheme},
    Modify, OpenApi, ToSchema,
};

use crate::controllers::{categories, medias, orders, products};

#[derive(OpenApi)]
#[openapi(
    paths(
        categories::create_category_controller,
        categories::get_all_categories_controller,
        categories::get_category_by_id_controller,
        categories::update_category_controller,
        categories::delete_category_controller,

        medias::create_media_controller,
        medias::get_all_medias_controller,
        medias::get_media_by_id_controller,
        medias::update_media_controller,
        medias::delete_media_controller,

        orders::create_checkout_for_self_controller,
        orders::create_checkout_as_guest_controller,
        orders::validate_checkout_controller,
        orders::get_all_checkouts_controller,
        orders::get_all_checkouts_for_self_controller,
        orders::get_all_orders_controller,
        orders::get_all_orders_for_self_controller,
        orders::get_order_by_id_controller,
        orders::update_order_controller,

        products::create_product_controller,
        products::get_all_products_controller,
        products::get_all_bestseller_products_controller,
        products::get_product_by_id_controller,
        products::update_product_controller,
        products::delete_product_controller,
    ),
    components(
        schemas(
            NewCategory, CategoryList, Category, UpdateCategory,
            NewCheckout, Checkout, CheckoutGuest, CustomerId,
            NewMarketPrice, NewSupplierPrice,
            NewMedia, MediaList, Media, MediaTypes, UpdateMedia,
            OrderList, Order, UpdateOrder, OrderStatus, Checkout, ValidateCheckout,
            NewOrderItem, OrderItem,
            NewProduct, ProductId, ProductList, Product, UpdateProduct, Specify, Association, CategoryRecursive,
            NewOrRelateAttribute, Attribute, NewAttribute, NewOrRelateAttribute,
            NewOrRelateAttributeOption, AttributeOptionId, AttributeOption, NewAttributeOption
        ),
    ),
    modifiers(&SecurityAddon),
    tags(
        (name = "categories", description = "Represents an category."),
        (name = "medias", description = "Represents a media."),
        (name = "orders", description = "Represents an order placed by a customer."),
        (name = "products", description = "Represents a generic product in the system."),
    ),
)]
pub(crate) struct ApiDoc;

pub(crate) struct SecurityAddon;

impl Modify for SecurityAddon {
    fn modify(&self, openapi: &mut utoipa::openapi::OpenApi) {
        if let Some(components) = openapi.components.as_mut() {
            components.add_security_scheme(
                "bearer",
                SecurityScheme::Http(
                    HttpBuilder::new()
                        .scheme(HttpAuthScheme::Bearer)
                        .bearer_format("JWT")
                        .build(),
                ),
            );
        }
    }
}

// TODO: There is no better way to implement it right now, see: https://github.com/juhaku/utoipa/issues/503
/// A generic struct that represents a paginated list of data.
#[allow(dead_code)]
#[derive(ToSchema)]
#[aliases(
    T = String,
    CategoryList = MyPagination<Category>,
    MediaList = MyPagination<Media>,
    CheckoutList = MyPagination<Checkout>,
    OrderList = MyPagination<Order>,
    ProductList = MyPagination<Product>,
)]
struct MyPagination<T> {
    /// An array of items of type `T` containing the current page of data.
    pub data: Vec<T>,
    /// The current page number.
    #[schema(example = "1")]
    pub current_page: u64,
    /// The total number of pages available.
    #[schema(example = "100")]
    pub total_pages: u64,
    /// The maximum number of items returned per page.
    #[schema(example = "20")]
    pub page_size: u64,
    /// The total number of items available across all pages.
    #[schema(example = "104")]
    pub total_data: u64,
}
