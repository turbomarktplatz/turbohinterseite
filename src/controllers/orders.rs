/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
use axum::{
    extract::{Path, Query, State},
    http::StatusCode,
    response::IntoResponse,
    Json,
};
use axum_extra::{
    headers::{authorization::Bearer, Authorization},
    TypedHeader,
};
use model::{
    Checkout, CheckoutGuest, CustomerId, NewCheckout, NewCustomerCheckout, OrderId, UpdateOrder,
    ValidateCheckout,
};
use pagination::prelude::*;
use rauthy_client::principal::PrincipalOidc;
use serde::Deserialize;

use crate::{
    error::NotFoundError, responses::AuthorizationResponse, services::OrderService, ApiResult,
    AppData,
};

#[utoipa::path(
    post,
    path = "/checkout/self",
    request_body = NewCustomerCheckout,
    responses(
        AuthorizationResponse,
        (status = 201, description = "Create a new checkout", body = Checkout),
    ),
    security(
        ("bearer" = []),
    ),
)]
pub(crate) async fn create_checkout_for_self_controller(
    State(app_data): State<AppData>,
    principal: PrincipalOidc,
    TypedHeader(bearer): TypedHeader<Authorization<Bearer>>,
    Json(payload): Json<NewCustomerCheckout>,
) -> ApiResult<impl IntoResponse> {
    #[derive(Debug, Deserialize)]
    struct UserInfoResponse {
        email: String,
        given_name: String,
        family_name: String,
    }
    let response = reqwest::Client::new()
        .get(format!("{}/oidc/userinfo", app_data.issuer_url))
        .bearer_auth(bearer.token())
        .send()
        .await
        .unwrap()
        .json::<UserInfoResponse>()
        .await
        .unwrap();
    let user_id = principal.id;
    let customer_id = CustomerId::new(user_id);
    let checkout: Checkout = app_data
        .checkout_for_user(
            payload,
            response.email,
            response.given_name,
            response.family_name,
            customer_id,
        )
        .await?;
    Ok(Json(checkout).into_response())
}

#[utoipa::path(
    post,
    path = "/checkout/guest",
    request_body = NewCheckout,
    responses(
        AuthorizationResponse,
        (status = 201, description = "Create a new checkout as guest", body = Checkout),
    ),
)]
pub(crate) async fn create_checkout_as_guest_controller(
    State(app_data): State<AppData>,
    Json(payload): Json<NewCheckout>,
) -> ApiResult<impl IntoResponse> {
    let checkout: CheckoutGuest = app_data.checkout_as_guest(payload).await?;
    Ok(Json(checkout).into_response())
}

#[utoipa::path(
    post,
    path = "/checkout/validate",
    request_body = ValidateCheckout,
    responses(
        (status = 201, description = "Validate a checkout", body = Order),
    ),
)]
pub(crate) async fn validate_checkout_controller(
    State(app_data): State<AppData>,
    Json(payload): Json<ValidateCheckout>,
) -> ApiResult<impl IntoResponse> {
    let order = app_data.validate_checkout(payload).await?;
    Ok(Json(order).into_response())
}

#[utoipa::path(
    get,
    path = "/checkout",
    params(PaginationQuery),
    responses(
        AuthorizationResponse,
        (status = 200, description = "List all Checkout ", body = CheckoutList),
    ),
    security(
        ("bearer" = []),
    ),
)]
pub(crate) async fn get_all_checkouts_controller(
    State(app_data): State<AppData>,
    principal: PrincipalOidc,
    Query(pagination): Query<PaginationQuery>,
) -> ApiResult<impl IntoResponse> {
    if !principal.is_admin {
        return Ok(StatusCode::FORBIDDEN.into_response());
    }
    get_in_pagination_controller(
        |pagination| app_data.find_checkout_in_page(pagination),
        pagination,
    )
    .await
    .map(IntoResponse::into_response)
}

#[utoipa::path(
    get,
    path = "/checkout/self",
    params(PaginationQuery),
    responses(
        AuthorizationResponse,
        (status = 200, description = "List all Checkouts for self", body = CheckoutList),
    ),
    security(
        ("bearer" = []),
    ),
)]
pub(crate) async fn get_all_checkouts_for_self_controller(
    State(app_data): State<AppData>,
    principal: PrincipalOidc,
    Query(pagination): Query<PaginationQuery>,
) -> ApiResult<impl IntoResponse> {
    let user_id = principal.id;
    get_in_pagination_controller(
        |pagination| app_data.find_checkout_by_customer_in_page(user_id.clone(), pagination),
        pagination,
    )
    .await
    .map(IntoResponse::into_response)
}

#[utoipa::path(
    get,
    path = "/orders",
    params(PaginationQuery),
    responses(
        AuthorizationResponse,
        (status = 200, description = "List all Orders", body = OrderList),
    ),
    security(
        ("bearer" = []),
    ),
)]
pub(crate) async fn get_all_orders_controller(
    State(app_data): State<AppData>,
    principal: PrincipalOidc,
    Query(pagination): Query<PaginationQuery>,
) -> ApiResult<impl IntoResponse> {
    if !principal.is_admin {
        return Ok(StatusCode::FORBIDDEN.into_response());
    }
    get_in_pagination_controller(
        |pagination| app_data.find_order_in_page(pagination),
        pagination,
    )
    .await
    .map(IntoResponse::into_response)
}

#[utoipa::path(
    get,
    path = "/orders/self",
    params(PaginationQuery),
    responses(
        AuthorizationResponse,
        (status = 200, description = "List all Orders for self", body = OrderList),
    ),
    security(
        ("bearer" = []),
    ),
)]
pub(crate) async fn get_all_orders_for_self_controller(
    State(app_data): State<AppData>,
    principal: PrincipalOidc,
    Query(pagination): Query<PaginationQuery>,
) -> ApiResult<impl IntoResponse> {
    let customer_id = CustomerId::new(principal.id);
    get_in_pagination_controller(
        |pagination| app_data.find_order_by_customer_in_page(customer_id.clone(), pagination),
        pagination,
    )
    .await
    .map(IntoResponse::into_response)
}

#[utoipa::path(
    get,
    path = "/orders/{order_id}",
    params(
        ("order_id" = String, Path, description = "Order ID"),
    ),
    responses(
        AuthorizationResponse,
        (status = 200, description = "Get Order by ID", body = Order),
    ),
    security(
        ("bearer" = []),
    ),
)]
pub(crate) async fn get_order_by_id_controller(
    State(app_data): State<AppData>,
    principal: PrincipalOidc,
    Path(order_id): Path<String>,
) -> ApiResult<impl IntoResponse> {
    let order_id = OrderId::new(order_id);
    let customer_id = CustomerId::new(principal.id.clone());
    let order = if principal.is_admin {
        app_data
            .find_order_by_id(order_id)
            .await?
            .ok_or(NotFoundError::Order)?
    } else {
        app_data
            .find_order_by_id_and_customer(order_id, customer_id)
            .await?
            .ok_or(NotFoundError::Order)?
    };
    Ok(Json(order).into_response())
}

#[utoipa::path(
    put,
    path = "/orders/{order_id}",
    params(
        ("order_id" = String, Path, description = "Order ID"),
    ),
    request_body = UpdateOrder,
    responses(
        AuthorizationResponse,
        (status = 200, description = "Update Order by ID", body = Order),
    ),
    security(
        ("bearer" = []),
    ),
)]
pub(crate) async fn update_order_controller(
    State(app_data): State<AppData>,
    principal: PrincipalOidc,
    Path(order_id): Path<String>,
    Json(payload): Json<UpdateOrder>,
) -> ApiResult<impl IntoResponse> {
    if !principal.is_admin {
        return Ok(StatusCode::FORBIDDEN.into_response());
    }
    let order_id = OrderId::new(order_id);
    update_controller(
        || app_data.find_order_by_id(order_id.clone()),
        || app_data.update_order(order_id.clone(), payload.clone()),
        NotFoundError::Order.into(),
    )
    .await
    .map(IntoResponse::into_response)
}
