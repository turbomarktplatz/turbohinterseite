/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
use axum::{
    extract::{Path, Query, State},
    http::StatusCode,
    response::IntoResponse,
    Json,
};
use model::{MediaId, NewMedia, UpdateMedia};
use pagination::prelude::*;
use rauthy_client::principal::PrincipalOidc;

use crate::{
    error::NotFoundError, responses::AuthorizationResponse, services::MediaService, ApiResult,
    AppData,
};

#[utoipa::path(
    post,
    path = "/medias",
    request_body = NewMedia,
    responses(
        AuthorizationResponse,
        (status = 201, description = "Create a new Media", body = Media),
    ),
    security(
        ("bearer" = []),
    ),
)]
pub(crate) async fn create_media_controller(
    State(app_data): State<AppData>,
    principal: PrincipalOidc,
    Json(payload): Json<NewMedia>,
) -> ApiResult<impl IntoResponse> {
    if !principal.is_admin {
        return Ok(StatusCode::FORBIDDEN.into_response());
    }
    app_data
        .create_media(payload.clone())
        .await
        .map(|data| (StatusCode::CREATED, Json(data)).into_response())
}

#[utoipa::path(
    get,
    path = "/medias",
    params(PaginationQuery),
    responses(
        (status = 200, description = "List all Medias", body = MediaList),
    ),
)]
pub(crate) async fn get_all_medias_controller(
    State(app_data): State<AppData>,
    Query(pagination): Query<PaginationQuery>,
) -> ApiResult<impl IntoResponse> {
    get_in_pagination_controller(
        |pagination| app_data.find_media_in_page(pagination),
        pagination,
    )
    .await
    .map(IntoResponse::into_response)
}

#[utoipa::path(
    get,
    path = "/medias/{media_id}",
    params(
        ("media_id" = String, Path, description = "Media ID"),
    ),
    responses(
        (status = 200, description = "Get Media by ID", body = Media),
    ),
)]
pub(crate) async fn get_media_by_id_controller(
    State(app_data): State<AppData>,
    Path(media_id): Path<MediaId>,
) -> ApiResult<impl IntoResponse> {
    app_data
        .find_media_by_id(media_id)
        .await?
        .map(|data| Json(data))
        .ok_or(NotFoundError::Media.into())
}

#[utoipa::path(
    put,
    path = "/medias/{media_id}",
    params(
        ("media_id" = String, Path, description = "Media ID"),
    ),
    request_body = UpdateMedia,
    responses(
        AuthorizationResponse,
        (status = 200, description = "Update Media by ID", body = Media),
    ),
    security(
        ("bearer" = []),
    ),
)]
pub(crate) async fn update_media_controller(
    State(app_data): State<AppData>,
    principal: PrincipalOidc,
    Path(id): Path<MediaId>,
    Json(payload): Json<UpdateMedia>,
) -> ApiResult<impl IntoResponse> {
    if !principal.is_admin {
        return Ok(StatusCode::FORBIDDEN.into_response());
    }
    update_controller(
        || app_data.find_media_by_id(id.clone()),
        || app_data.update_media(id.clone(), payload.clone()),
        NotFoundError::Media.into(),
    )
    .await
    .map(IntoResponse::into_response)
}

#[utoipa::path(
    delete,
    path = "/medias/{media_id}",
    params(
        ("media_id" = String, Path, description = "Media ID"),
    ),
    responses(
        AuthorizationResponse,
        (status = 200, description = "Delete Media by ID"),
    ),
    security(
        ("bearer" = []),
    ),
)]
pub(crate) async fn delete_media_controller(
    State(app_data): State<AppData>,
    principal: PrincipalOidc,
    Path(media_id): Path<MediaId>,
) -> ApiResult<impl IntoResponse> {
    if !principal.is_admin {
        return Ok(StatusCode::FORBIDDEN.into_response());
    }
    delete_controller(
        || app_data.find_media_by_id(media_id.clone()),
        || app_data.delete_media(media_id.clone()),
        NotFoundError::Media.into(),
    )
    .await
    .map(IntoResponse::into_response)
}
