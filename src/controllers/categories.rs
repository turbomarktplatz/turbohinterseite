/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
use axum::{
    extract::{Path, Query, State},
    http::StatusCode,
    response::IntoResponse,
    Json,
};
use model::{CategoryId, NewCategory, UpdateCategory};
use pagination::prelude::*;
use rauthy_client::principal::PrincipalOidc;
use serde::{Deserialize, Serialize};
use utoipa::{IntoParams, ToSchema};

use crate::{
    error::NotFoundError, responses::AuthorizationResponse, services::*, ApiResult, AppData,
};

#[utoipa::path(
    post,
    path = "/categories",
    request_body = NewCategory,
    responses(
        AuthorizationResponse,
        (status = 201, description = "Create a new Category", body = Category),
    ),
    security(
        ("bearer" = []),
    ),
)]
pub(crate) async fn create_category_controller(
    State(app_data): State<AppData>,
    principal: PrincipalOidc,
    Json(payload): Json<NewCategory>,
) -> ApiResult<impl IntoResponse> {
    if !principal.is_admin {
        return Ok(StatusCode::FORBIDDEN.into_response());
    }
    app_data
        .create_category(payload.clone())
        .await
        .map(|data| (StatusCode::CREATED, Json(data)).into_response())
}

#[derive(Debug, Clone, Default, Deserialize, Serialize, IntoParams, ToSchema)]
#[serde(default)]
#[into_params(style = Form, parameter_in = Query)]
pub(crate) struct ListAllCategoriesQuery {
    #[schema(value_type = String)]
    #[schema(example = "1o2ailu2nbbhsn2prbq7")]
    pub(crate) parent_id: Option<CategoryId>,
}

#[utoipa::path(
    get,
    path = "/categories",
    params(PaginationQuery, ListAllCategoriesQuery),
    responses(
        (status = 200, description = "List all Categories", body = CategoryList),
    ),
)]
pub(crate) async fn get_all_categories_controller(
    State(app_data): State<AppData>,
    Query(pagination): Query<PaginationQuery>,
    Query(list_all_categories): Query<ListAllCategoriesQuery>,
) -> ApiResult<impl IntoResponse> {
    get_in_pagination_controller(
        |pagination| app_data.find_category_in_page(pagination, &list_all_categories),
        pagination,
    )
    .await
    .map(IntoResponse::into_response)
}

#[utoipa::path(
    get,
    path = "/categories/{category_id}",
    params(
        ("category_id" = String, Path, description = "Category ID"),
    ),
    responses(
        (status = 200, description = "Get Category by ID", body = Category),
    ),
)]
pub(crate) async fn get_category_by_id_controller(
    State(app_data): State<AppData>,
    Path(category_id): Path<CategoryId>,
) -> ApiResult<impl IntoResponse> {
    app_data
        .find_category_by_id(category_id)
        .await?
        .map(|data| Json(data))
        .ok_or(NotFoundError::Category.into())
}

#[utoipa::path(
    put,
    path = "/categories/{category_id}",
    params(
        ("category_id" = String, Path, description = "Category ID"),
    ),
    request_body = UpdateCategory,
    responses(
        AuthorizationResponse,
        (status = 200, description = "Update Category by ID", body = Category),
    ),
    security(
        ("bearer" = []),
    ),
)]
pub(crate) async fn update_category_controller(
    State(app_data): State<AppData>,
    principal: PrincipalOidc,
    Path(id): Path<CategoryId>,
    Json(payload): Json<UpdateCategory>,
) -> ApiResult<impl IntoResponse> {
    if !principal.is_admin {
        return Ok(StatusCode::FORBIDDEN.into_response());
    }
    update_controller(
        || app_data.find_category_by_id(id.clone()),
        || app_data.update_category(id.clone(), payload.clone()),
        NotFoundError::Category.into(),
    )
    .await
    .map(IntoResponse::into_response)
}

#[utoipa::path(
    delete,
    path = "/categories/{category_id}",
    params(
        ("category_id" = String, Path, description = "Category ID"),
    ),
    responses(
        AuthorizationResponse,
        (status = 200, description = "Delete Category by ID"),
    ),
    security(
        ("bearer" = []),
    ),
)]
pub(crate) async fn delete_category_controller(
    State(app_data): State<AppData>,
    principal: PrincipalOidc,
    Path(category_id): Path<CategoryId>,
) -> ApiResult<impl IntoResponse> {
    if !principal.is_admin {
        return Ok(StatusCode::FORBIDDEN.into_response());
    }
    delete_controller(
        || app_data.find_category_by_id(category_id.clone()),
        || app_data.delete_category(category_id.clone()),
        NotFoundError::Category.into(),
    )
    .await
    .map(IntoResponse::into_response)
}
