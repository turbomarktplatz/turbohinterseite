/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
use axum::{
    extract::{Path, Query, State},
    http::StatusCode,
    response::IntoResponse,
    Json,
};
use model::{CategoryId, NewProduct, ProductId, UpdateProduct};
use pagination::prelude::*;
use rauthy_client::principal::PrincipalOidc;
use serde::{Deserialize, Serialize};
use utoipa::{IntoParams, ToSchema};

use crate::{
    error::{AllreadyExistsError, NotFoundError},
    responses::AuthorizationResponse,
    services::*,
    ApiResult, AppData,
};

#[utoipa::path(
    post,
    path = "/products",
    request_body = NewProduct,
    responses(
        AuthorizationResponse,
        (status = 201, description = "Create a new Product", body = Product),
    ),
    security(
        ("bearer" = []),
    ),
)]
pub(crate) async fn create_product_controller(
    State(app_data): State<AppData>,
    principal: PrincipalOidc,
    Json(payload): Json<NewProduct>,
) -> ApiResult<impl IntoResponse> {
    if !principal.is_admin {
        return Ok(StatusCode::FORBIDDEN.into_response());
    }
    create_controller(
        || app_data.find_product_by_id(payload.id.clone().into()),
        || app_data.create_product(payload.clone()),
        AllreadyExistsError::Product.into(),
    )
    .await
    .map(IntoResponse::into_response)
}

#[derive(Debug, Clone, Default, Deserialize, Serialize, IntoParams, ToSchema)]
#[serde(default)]
#[into_params(style = Form, parameter_in = Query)]
pub(crate) struct ListAllProductsQuery {
    #[schema(value_type = String)]
    pub(crate) category_id: Option<CategoryId>,
}

#[utoipa::path(
    get,
    path = "/products",
    params(PaginationQuery, ListAllProductsQuery),
    responses(
        (status = 200, description = "List all Products", body = ProductList),
    ),
)]
pub(crate) async fn get_all_products_controller(
    State(app_data): State<AppData>,
    Query(pagination): Query<PaginationQuery>,
    Query(list_all_products): Query<ListAllProductsQuery>,
) -> ApiResult<impl IntoResponse> {
    get_in_pagination_controller(
        |pagination| app_data.find_product_in_page(pagination, &list_all_products),
        pagination,
    )
    .await
    .map(IntoResponse::into_response)
}

#[utoipa::path(
    get,
    path = "/products/bestseller",
    params(PaginationQuery),
    responses(
        (status = 200, description = "List all bestseller Products", body = ProductList),
    ),
)]
pub(crate) async fn get_all_bestseller_products_controller(
    State(app_data): State<AppData>,
    Query(pagination): Query<PaginationQuery>,
) -> ApiResult<impl IntoResponse> {
    get_in_pagination_controller(
        |pagination| app_data.find_bestseller_products(pagination),
        pagination,
    )
    .await
    .map(IntoResponse::into_response)
}

#[utoipa::path(
    get,
    path = "/products/sitemap",
    responses(
        (status = 200, description = "List all sitemap Products", body = ProductSitemapList),
    ),
)]
pub(crate) async fn get_all_sitemap_product_controller(
    State(app_data): State<AppData>,
) -> ApiResult<impl IntoResponse> {
    app_data
        .find_sitemap_products()
        .await
        .map(|products| Json(products))
}

#[utoipa::path(
    get,
    path = "/products/ids",
    params(PaginationQuery, ListAllProductsQuery),
    responses(
        (status = 200, description = "List all Products Ids", body = ProductIdList),
    ),
)]
pub(crate) async fn get_all_product_ids_controller(
    State(app_data): State<AppData>,
) -> ApiResult<impl IntoResponse> {
    app_data.find_all_product_ids().await.map(|data| Json(data))
}

#[utoipa::path(
    get,
    path = "/products/{product_id}",
    params(
        ("product_id" = String, Path, description = "Product ID"),
    ),
    responses(
        (status = 200, description = "Get Product by Provider and ID", body = Product),
    ),
    security(
        ("{}" = [], "bearer" = []),
    ),
)]
pub(crate) async fn get_product_by_id_controller(
    State(app_data): State<AppData>,
    Path(product_id): Path<ProductId>,
) -> ApiResult<impl IntoResponse> {
    app_data
        .find_product_by_id(product_id.into())
        .await?
        .map(|data| Json(data))
        .ok_or(NotFoundError::Product.into())
}

#[utoipa::path(
    get,
    path = "/products/{product_id}/exists",
    params(
        ("product_id" = String, Path, description = "Product ID"),
    ),
    responses(
        (status = 200, description = "Check if product exists", body = bool),
    ),
    security(
        ("{}" = [], "bearer" = []),
    ),
)]
pub(crate) async fn check_product_exists_controller(
    State(app_data): State<AppData>,
    Path(product_id): Path<ProductId>,
) -> ApiResult<impl IntoResponse> {
    app_data
        .check_product_exists(product_id.into())
        .await?
        .map(|data| {
            if data {
                StatusCode::OK
            } else {
                StatusCode::NOT_FOUND
            }
        })
        .ok_or(NotFoundError::Product.into())
}

#[utoipa::path(
    put,
    path = "/products/{product_id}",
    params(
        ("product_id" = String, Path, description = "Product ID"),
    ),
    request_body = UpdateProduct,
    responses(
        AuthorizationResponse,
        (status = 200, description = "Update Product by ID", body = Product),
    ),
    security(
        ("bearer" = []),
    ),
)]
pub(crate) async fn update_product_controller(
    State(app_data): State<AppData>,
    principal: PrincipalOidc,
    Path(product_id): Path<ProductId>,
    Json(payload): Json<UpdateProduct>,
) -> ApiResult<impl IntoResponse> {
    if !principal.is_admin {
        return Ok(StatusCode::FORBIDDEN.into_response());
    }
    update_controller(
        || app_data.find_product_by_id(product_id.clone()),
        || app_data.update_product(product_id.clone(), payload.clone()),
        NotFoundError::Product.into(),
    )
    .await
    .map(IntoResponse::into_response)
}

#[utoipa::path(
    delete,
    path = "/products/{product_id}",
    params(
        ("product_id" = String, Path, description = "Product ID"),
    ),
    responses(
        AuthorizationResponse,
        (status = 200, description = "Delete Product by ID"),
    ),
    security(
        ("bearer" = []),
    ),
)]
pub(crate) async fn delete_product_controller(
    State(app_data): State<AppData>,
    principal: PrincipalOidc,
    Path(product_id): Path<ProductId>,
) -> ApiResult<impl IntoResponse> {
    if !principal.is_admin {
        return Ok(StatusCode::FORBIDDEN.into_response());
    }
    delete_controller(
        || app_data.find_product_by_id(product_id.clone()),
        || app_data.delete_product(product_id.clone()),
        NotFoundError::Product.into(),
    )
    .await
    .map(IntoResponse::into_response)
}
