/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use axum::{
    http::{
        header::{self, InvalidHeaderValue},
        HeaderValue, Method,
    },
    Router,
};
use log::info;
use opentelemetry::{global::shutdown_logger_provider, metrics::MeterProvider};
use opentelemetry_appender_tracing::layer::OpenTelemetryTracingBridge;
use rauthy_client::{
    oidc_config::{ClaimMapping, JwtClaim, JwtClaimTyp, RauthyConfig},
    provider::OidcProvider,
};
use serde::{Deserialize, Deserializer};
use std::{collections::HashSet, net::SocketAddr};
use surrealdb::{opt::auth::Database, Surreal};
use tower_http::cors::CorsLayer;
use tracing::Level;
use tracing_subscriber::{prelude::__tracing_subscriber_SubscriberExt, util::SubscriberInitExt};
use utoipa::OpenApi;
use utoipa_swagger_ui::SwaggerUi;

mod api;
mod controllers;
mod error;
mod rate_limit;
mod responses;
mod routes;
mod services;
mod surrealdb_fix;
mod telemetry;

use crate::error::ApiError;

pub(crate) type ApiResult<T> = Result<T, ApiError>;

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub(crate) struct Config {
    public_url: String,

    issuer_url: String,
    client_id: String,
    client_secret: String,
    allowed_audiences: HashSet<String>,

    surrealdb_host: String,
    surrealdb_username: String,
    surrealdb_password: String,
    surrealdb_namespace: String,
    surrealdb_database: String,

    #[serde(rename = "OTEL_SERVICE_NAME")]
    #[serde(default = "default_service_name")]
    service_name: String,
    #[serde(rename = "OTEL_SERVICE_ENVIRONMENT")]
    #[serde(default = "default_service_environment")]
    service_environment: String,
    #[serde(rename = "OTEL_LOGGING_ENDPOINT")]
    logging_endpoint: Option<String>,
    #[serde(rename = "OTEL_METRICS_ENDPOINT")]
    metrics_endpoint: Option<String>,
    #[serde(rename = "OTEL_TRACING_ENDPOINT")]
    tracing_endpoint: Option<String>,

    openapi_endpoint: String,
    openapi_description: String,

    #[serde(default)]
    #[serde(deserialize_with = "deserialize_allow_origin")]
    allow_origin: AllowOrigin,

    #[serde(default)]
    shipping_fees: Vec<ShippingFee>,

    stripe_key: String,
}

fn default_service_name() -> String {
    "turbohinterseite".to_owned()
}

fn default_service_environment() -> String {
    "local".to_owned()
}

#[derive(Debug, Clone, Deserialize)]
pub(crate) struct OpenTelemetryConfig {}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub(crate) struct ShippingFee {
    pub(crate) min: i32,
    pub(crate) max: i32,
    pub(crate) price: i32,
}

fn deserialize_allow_origin<'de, D>(deserializer: D) -> Result<AllowOrigin, D::Error>
where
    D: Deserializer<'de>,
{
    let value = String::deserialize(deserializer)?;
    let allow_origin = match value {
        value if value.as_str() == "*" => AllowOrigin::Any,
        value if value.contains(',') => {
            AllowOrigin::List(value.split(',').map(String::from).collect())
        }
        value => AllowOrigin::Exact(value),
    };

    Ok(allow_origin)
}

#[derive(Debug, Clone, Deserialize)]
#[serde(untagged)]
enum AllowOrigin {
    Any,
    List(Vec<String>),
    Exact(String),
}

impl Default for AllowOrigin {
    fn default() -> Self {
        Self::List(vec![])
    }
}

impl TryInto<tower_http::cors::AllowOrigin> for AllowOrigin {
    type Error = InvalidHeaderValue;

    fn try_into(self) -> Result<tower_http::cors::AllowOrigin, Self::Error> {
        let allow_origin = match self {
            Self::Any => tower_http::cors::AllowOrigin::any(),
            Self::List(origins) => {
                let origins = origins
                    .iter()
                    .map(|origin| HeaderValue::from_str(origin.as_str()))
                    .collect::<Result<Vec<HeaderValue>, Self::Error>>()?;
                tower_http::cors::AllowOrigin::list(origins)
            }
            Self::Exact(origin) => {
                let origin = HeaderValue::from_str(origin.as_str())?;
                tower_http::cors::AllowOrigin::exact(origin)
            }
        };
        Ok(allow_origin)
    }
}

#[derive(Clone)]
pub(crate) struct AppData {
    pub(crate) public_url: String,
    pub(crate) surreal: Surreal<surrealdb::engine::any::Any>,
    pub(crate) stripe: stripe::Client,
    pub(crate) shipping_fees: Vec<ShippingFee>,
    pub(crate) issuer_url: String,
}

#[allow(clippy::too_many_lines)]
#[tokio::main]
#[snafu::report]
async fn main() -> ApiResult<()> {
    // AppConfig
    let config: Config = envious::Config::default()
        .without_prefix()
        .build_from_env()
        .expect("Could not deserialize config from env");

    // Tracing
    let tracer = config.tracing_endpoint.clone().map(|tracing_endpoint| {
        telemetry::init_tracing(
            config.service_name.clone(),
            config.service_environment.clone(),
            tracing_endpoint,
        )
    });

    // Logging
    let logging_provider = config.logging_endpoint.map(|logging_endpoint| {
        telemetry::init_logging(
            config.service_name.clone(),
            config.service_environment.clone(),
            logging_endpoint,
        )
    });

    // Metrics
    let meter_provider = config.metrics_endpoint.clone().map(|metrics_endpoint| {
        telemetry::init_metrics(
            config.service_name.clone(),
            config.service_environment.clone(),
            metrics_endpoint,
        )
    });

    let subscriber = tracing_subscriber::fmt::Subscriber::builder()
        .pretty()
        .with_ansi(true)
        .with_max_level(Level::TRACE)
        .finish();

    match (tracer.clone(), &logging_provider) {
        (Some(tracer), Some(logging_provider)) => subscriber
            .with(tracing_subscriber::EnvFilter::from_default_env())
            .with(tracing_opentelemetry::layer().with_tracer(tracer))
            .with(OpenTelemetryTracingBridge::new(logging_provider))
            .try_init(),
        (Some(tracer), None) => subscriber
            .with(tracing_subscriber::EnvFilter::from_default_env())
            .with(tracing_opentelemetry::layer().with_tracer(tracer))
            .try_init(),
        (None, Some(logging_provider)) => subscriber
            .with(tracing_subscriber::EnvFilter::from_default_env())
            .with(OpenTelemetryTracingBridge::new(logging_provider))
            .try_init(),
        (None, None) => subscriber
            .with(tracing_subscriber::EnvFilter::from_default_env())
            .try_init(),
    }
    .expect("unable to init tracing subscriber");

    // Stripe
    let stripe = stripe::Client::new(config.stripe_key);

    let surreal = surrealdb::engine::any::connect(config.surrealdb_host)
        .await
        .expect("unable to connect to surrealdb");

    surreal
        .signin(Database {
            username: &config.surrealdb_username,
            password: &config.surrealdb_password,
            namespace: &config.surrealdb_namespace,
            database: &config.surrealdb_database,
        })
        .await
        .expect("unable to signin to surrealdb");

    surreal
        .use_ns(config.surrealdb_namespace)
        .use_db(config.surrealdb_database)
        .await
        .expect("unable to connect to database in surrealdb");

    rauthy_client::init().await.unwrap();

    let rauthy_config = RauthyConfig {
        admin_claim: ClaimMapping::Or(vec![JwtClaim {
            typ: JwtClaimTyp::Roles,
            value: "admin".to_string(),
        }]),
        user_claim: ClaimMapping::Any,
        allowed_audiences: config.allowed_audiences,
        email_verified: true,
        iss: config.issuer_url.clone(),
        scope: vec![
            "openid".to_string(),
            "email".to_string(),
            "profile".to_string(),
            "groups".to_string(),
        ],
        client_id: config.client_id,
        secret: Some(config.client_secret),
    };
    OidcProvider::setup_from_config(
        rauthy_config,
        format!("{}/callback", config.public_url).as_str(),
    )
    .await
    .unwrap();

    // AppData
    let app_data = AppData {
        public_url: config.public_url,
        surreal,
        stripe,
        shipping_fees: config.shipping_fees,
        issuer_url: config.issuer_url,
    };

    // OpenAPI
    let mut openapi = api::ApiDoc::openapi();
    let mut server = utoipa::openapi::Server::new(config.openapi_endpoint);
    server.description = Some(config.openapi_description);
    openapi.servers = Some(vec![server]);

    // App
    let app = Router::new()
        .merge(SwaggerUi::new("/docs").url("/docs/openapi.json", openapi))
        .merge(routes::routes())
        .with_state(app_data);

    // Tracer
    let app = if config.tracing_endpoint.is_some() {
        app.layer(
            tower_http::trace::TraceLayer::new_for_http()
                .make_span_with(tower_http::trace::DefaultMakeSpan::new())
                .on_response(tower_http::trace::DefaultOnResponse::new()),
        )
    } else {
        app
    };

    // Metrics
    let app = if let Some(meter_provider) = &meter_provider {
        app.route_layer(axum::middleware::from_fn_with_state(
            std::sync::Arc::new(telemetry::MetricsData::new(
                config.service_environment,
                meter_provider.meter(config.service_name),
            )),
            telemetry::track_metrics,
        ))
    } else {
        app
    };

    // CORS
    let allow_origin: tower_http::cors::AllowOrigin = config
        .allow_origin
        .try_into()
        .expect("unable to parse 'ALLOW_ORIGINS'");
    let cors = CorsLayer::new()
        .allow_methods([Method::GET, Method::POST, Method::PUT, Method::DELETE])
        .allow_headers(vec![
            header::ACCEPT,
            header::ACCEPT_ENCODING,
            header::ACCEPT_LANGUAGE,
            header::ACCESS_CONTROL_ALLOW_HEADERS,
            header::ACCESS_CONTROL_ALLOW_METHODS,
            header::ACCESS_CONTROL_ALLOW_ORIGIN,
            header::AUTHORIZATION,
            header::CONTENT_TYPE,
        ])
        .allow_origin(allow_origin);
    let app = app.layer(cors);

    // Server
    let listener = tokio::net::TcpListener::bind("0.0.0.0:3001").await.unwrap();
    info!("listening on {}", 3001);

    axum::serve(
        listener,
        app.into_make_service_with_connect_info::<SocketAddr>(),
    )
    .await
    .unwrap();

    if tracer.is_some() {
        opentelemetry::global::shutdown_tracer_provider();
    }

    if logging_provider.is_some() {
        shutdown_logger_provider();
    }

    // if let Some(meter_provider) = meter_provider {
    //     meter_provider
    //         .shutdown()
    //         .expect("unable to shutdown meter_provider");
    // }

    Ok(())
}
