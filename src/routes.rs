/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use axum::{
    routing::{get, post, put},
    Router,
};
use governor::Quota;
use nonzero_ext::nonzero;
use tower::ServiceBuilder;

use crate::{controllers::*, rate_limit::RateLimitLayer, AppData};

// Rate Limit per minute
const DEFAULT_RATE_LIMIT: u32 = 600;
const DEFAULT_MAX_BURST: u32 = 100;
const CREATE_ORDERS_RATE_LIMIT: u32 = 40;
const CREATE_ORDERS_MAX_BURST: u32 = 10;
const CREATE_CHECKOUT_RATE_LIMIT: u32 = 20;
const CREATE_CHECKOUT_MAX_BURST: u32 = 5;
const VALIDATE_CHECKOUT_RATE_LIMIT: u32 = 20;
const VALIDATE_CHECKOUT_MAX_BURST: u32 = 5;

// struct Keys {
//     public_key: String,
//     audiences: Vec<String>,
//     algorithm: Algorithm,
// }

// static KEYS: Lazy<Keys> = Lazy::new(|| Keys {
//     public_key: std::env::var("AUTH_PUBLIC_KEY").expect("AUTH_PUBLIC_KEY must be set"),
//     audiences: std::env::var("AUTH_AUDIENCES")
//         .expect("AUTH_AUDIENCES must be set")
//         .split(',')
//         .map(ToOwned::to_owned)
//         .collect::<Vec<_>>(),
//     algorithm: Algorithm::from_str(
//         std::env::var("AUTH_ALGORITHM")
//             .expect("AUTH_ALGORITHM must be set")
//             .as_str(),
//     )
//     .expect("AUTH_ALGORITHM is invalid"),
// });

// #[derive(Debug, Clone, Deserialize, Serialize)]
// pub struct Claims {
//     pub sub: String,
//     pub email: String,
//     pub roles: Vec<String>,
// }

// impl Claims {
//     pub fn contains_group(&self, other: &str) -> bool {
//         self.roles.contains(&other.to_owned())
//     }
// }

// #[async_trait::async_trait]
// impl<S> FromRequestParts<S> for Claims
// where
//     S: Send + Sync,
// {
//     type Rejection = AuthError;

//     async fn from_request_parts(parts: &mut Parts, _state: &S) -> Result<Self, Self::Rejection> {
//         let TypedHeader(Authorization(bearer)) = parts
//             .extract::<TypedHeader<Authorization<Bearer>>>()
//             .await
//             .map_err(|_| AuthError::InvalidToken)?;
//         let mut validation = Validation::new(KEYS.algorithm);
//         validation.set_audience(&KEYS.audiences);
//         let token_data = decode::<Claims>(
//             bearer.token(),
//             &DecodingKey::from_ed_components(&KEYS.public_key)
//                 .map_err(|_| AuthError::InvalidAlgorithm)?,
//             &validation,
//         )
//         .map_err(|_| AuthError::InvalidToken)?;

//         Ok(token_data.claims)
//     }
// }

// impl IntoResponse for AuthError {
//     fn into_response(self) -> Response {
//         let (status, error_message) = match self {
//             AuthError::InvalidAlgorithm => (StatusCode::INTERNAL_SERVER_ERROR, "Invalid algorithm"),
//             AuthError::InvalidToken => (StatusCode::BAD_REQUEST, "Invalid token"),
//         };
//         let body = Json(json!({
//             "error": error_message,
//         }));
//         (status, body).into_response()
//     }
// }

// #[derive(Debug)]
// pub enum AuthError {
//     InvalidAlgorithm,
//     InvalidToken,
// }

pub(super) fn routes() -> Router<AppData> {
    Router::new()
        .route("/health", get(|| async move { "UP" }))
        .nest("/api", api())
        .layer(
            ServiceBuilder::new().layer(RateLimitLayer::new(
                Quota::per_minute(nonzero!(DEFAULT_RATE_LIMIT))
                    .allow_burst(nonzero!(DEFAULT_MAX_BURST)),
            )),
        )
}

fn api() -> Router<AppData> {
    Router::new()
        .merge(categories())
        .merge(medias())
        .merge(orders())
        .merge(products())
}

fn categories() -> Router<AppData> {
    Router::new().nest(
        "/categories",
        Router::new()
            .route(
                "/",
                post(create_category_controller).get(get_all_categories_controller),
            )
            .route(
                "/:category_id",
                put(update_category_controller)
                    .delete(delete_category_controller)
                    .get(get_category_by_id_controller),
            ),
    )
}

fn medias() -> Router<AppData> {
    Router::new().nest(
        "/medias",
        Router::new()
            .route(
                "/",
                post(create_media_controller).get(get_all_medias_controller),
            )
            .route(
                "/:media_id",
                put(update_media_controller)
                    .delete(delete_media_controller)
                    .get(get_media_by_id_controller),
            ),
    )
}

fn orders() -> Router<AppData> {
    Router::new()
        .nest(
            "/orders",
            Router::new()
                .route("/", get(get_all_orders_controller))
                .route("/self", get(get_all_orders_for_self_controller))
                .route(
                    "/:order_id",
                    get(get_order_by_id_controller).put(update_order_controller),
                ),
        )
        .nest(
            "/checkout",
            Router::new()
                .route("/", get(get_all_checkouts_controller))
                .route(
                    "/self",
                    get(get_all_checkouts_for_self_controller)
                        .post(create_checkout_for_self_controller)
                        .layer(
                            ServiceBuilder::new().layer(RateLimitLayer::new(
                                Quota::per_minute(nonzero!(CREATE_ORDERS_RATE_LIMIT))
                                    .allow_burst(nonzero!(CREATE_ORDERS_MAX_BURST)),
                            )),
                        ),
                )
                .route(
                    "/guest",
                    post(create_checkout_as_guest_controller).layer(
                        ServiceBuilder::new().layer(RateLimitLayer::new(
                            Quota::per_minute(nonzero!(CREATE_CHECKOUT_RATE_LIMIT))
                                .allow_burst(nonzero!(CREATE_CHECKOUT_MAX_BURST)),
                        )),
                    ),
                )
                .route(
                    "/validate",
                    post(validate_checkout_controller).layer(
                        ServiceBuilder::new().layer(RateLimitLayer::new(
                            Quota::per_minute(nonzero!(VALIDATE_CHECKOUT_RATE_LIMIT))
                                .allow_burst(nonzero!(VALIDATE_CHECKOUT_MAX_BURST)),
                        )),
                    ),
                ),
        )
}

fn products() -> Router<AppData> {
    Router::new().nest(
        "/products",
        Router::new()
            .route(
                "/",
                post(create_product_controller).get(get_all_products_controller),
            )
            .route("/sitemap", get(get_all_sitemap_product_controller))
            .route("/bestseller", get(get_all_bestseller_products_controller))
            .route("/ids", get(get_all_product_ids_controller))
            .nest(
                "/:product_id",
                Router::new()
                    .route(
                        "/",
                        put(update_product_controller)
                            .delete(delete_product_controller)
                            .get(get_product_by_id_controller),
                    )
                    .route("/exists", get(check_product_exists_controller)),
            ),
    )
}
