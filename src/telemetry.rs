/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
use axum::{
    extract::{MatchedPath, State},
    http::Request,
    middleware::Next,
    response::IntoResponse,
};
use opentelemetry::{
    metrics::{Counter, Meter, MeterProvider, ObservableGauge, Unit},
    KeyValue,
};
use opentelemetry_otlp::WithExportConfig;
use opentelemetry_sdk::{logs::LoggerProvider, runtime, trace::Tracer, Resource};
use std::{sync::Arc, time::Instant};
use tracing::instrument;

#[instrument]
pub(crate) fn init_tracing(
    service_name: String,
    service_environment: String,
    tracing_endpoint: String,
) -> Tracer {
    opentelemetry_otlp::new_pipeline()
        .tracing()
        .with_exporter(
            opentelemetry_otlp::new_exporter()
                .tonic()
                .with_endpoint(tracing_endpoint),
        )
        .with_trace_config(
            opentelemetry_sdk::trace::config().with_resource(Resource::new(vec![
                KeyValue::new("service.name", service_name),
                KeyValue::new("service.environment", service_environment),
            ])),
        )
        .with_batch_config(opentelemetry_sdk::trace::BatchConfig::default())
        .install_batch(runtime::Tokio)
        .expect("unable to init tracing")
}

#[instrument]
pub(crate) fn init_logging(
    service_name: String,
    service_environment: String,
    logging_endpoint: String,
) -> LoggerProvider {
    LoggerProvider::builder()
        .with_config(
            opentelemetry_sdk::logs::Config::default().with_resource(Resource::new(vec![
                KeyValue::new("service.name", service_name),
                KeyValue::new("service.environment", service_environment),
            ])),
        )
        .with_batch_exporter(
            opentelemetry_otlp::new_exporter()
                .tonic()
                .with_endpoint(logging_endpoint)
                .build_log_exporter()
                .unwrap(),
            opentelemetry_sdk::runtime::Tokio,
        )
        .build()
}

#[instrument]
pub(crate) fn init_metrics(
    service_name: String,
    service_environment: String,
    metrics_endpoint: String,
) -> impl MeterProvider {
    opentelemetry_otlp::new_pipeline()
        .metrics(runtime::Tokio)
        .with_exporter(
            opentelemetry_otlp::new_exporter()
                .tonic()
                .with_endpoint(metrics_endpoint),
        )
        .with_resource(Resource::new(vec![
            KeyValue::new("service.name", service_name.clone()),
            KeyValue::new("service.environment", service_environment),
        ]))
        .build()
        .expect("unable to init metrics")
}

pub(crate) struct MetricsData {
    pub(crate) service_environment: String,
    pub(crate) requests_controller: Counter<u64>,
    pub(crate) request_latencies_controller: ObservableGauge<f64>,
}

impl MetricsData {
    #[instrument]
    pub(crate) fn new(service_environment: String, meter: Meter) -> Self {
        let requests_controller = meter
            .u64_counter("requests_counter")
            .with_description("Counts the total number of requests.")
            .init();
        let request_latencies_controller = meter
            .f64_observable_gauge("requests_latency")
            .with_description("Measures the time it takes for a system to respond to a request.")
            .with_unit(Unit::new("ms"))
            .init();
        MetricsData {
            service_environment,
            requests_controller,
            request_latencies_controller,
        }
    }
}

#[instrument(skip_all)]
pub(crate) async fn track_metrics(
    State(metrics_data): State<Arc<MetricsData>>,
    req: Request<axum::body::Body>,
    next: Next,
) -> impl IntoResponse {
    let start = Instant::now();

    let method = req.method().clone();
    let path = if let Some(matched_path) = req.extensions().get::<MatchedPath>() {
        matched_path.as_str().to_owned()
    } else {
        req.uri().path().to_owned()
    };

    let response = next.run(req).await;

    let latency = start.elapsed().as_secs_f64();
    let status = response.status().as_u16().to_string();

    let tags = vec![
        KeyValue::new("environment", metrics_data.service_environment.clone()),
        KeyValue::new("method", method.to_string()),
        KeyValue::new("path", path),
        KeyValue::new("status", status),
    ];
    metrics_data.requests_controller.add(1, &tags);
    metrics_data
        .request_latencies_controller
        .observe(latency, &tags);

    response
}
