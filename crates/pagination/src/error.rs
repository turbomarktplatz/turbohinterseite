/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
use thiserror::Error;

/// An error that can occur while paginating data.
#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Error)]
pub enum PaginationError {
    /// The requested page number is negative or zero.
    #[error("page is negative or zero")]
    IsNegativeOrZero,
    /// The requested page size is too low.
    #[error("page size is too low")]
    SizeTooLow,
    /// The requested page size is too large.
    #[error("page size is too large")]
    SizeTooLarge,
}
