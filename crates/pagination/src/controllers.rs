/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use axum::{http::StatusCode, response::IntoResponse, Json};
use futures::Future;

use crate::{
    error::PaginationError,
    models::{Pagination, PaginationQuery},
};

/// Returns a controller that fetches data in a paginated format.
///
/// # Type parameters
///
/// - `FindInPaginationFunction`: The type of the function that fetches the data.
/// - `FindInPaginationFunctionFuture`: The type of the future returned by the function that
///   fetches the data.
/// - `Model`: The type of the model to be paginated.
/// - `Error`: The type of error that can occur while fetching the data.
///
/// # Arguments
///
/// - `fn_find_in_pagination`: A function that fetches the data in a paginated format.
/// - `pagination_query`: The query parameters for pagination.
///
/// # Returns
///
/// A `Result` containing a `Json` object with the paginated data and an HTTP status code of 200,
/// or an error if the pagination query parameters are invalid.
///
/// # Errors
///
/// This can fail, if the pagination is out of it's maximum values
/// or if the find function throws an error.
pub async fn get_in_pagination_controller<
    FindInPaginationFunction,
    FindInPaginationFunctionFuture,
    Model: std::fmt::Debug,
    Error: std::error::Error + From<PaginationError>,
>(
    fn_find_in_pagination: FindInPaginationFunction,
    pagination_query: PaginationQuery,
) -> Result<impl IntoResponse, Error>
where
    FindInPaginationFunction: Fn(PaginationQuery) -> FindInPaginationFunctionFuture,
    FindInPaginationFunctionFuture: Future<Output = Result<Pagination<Model>, Error>>,
    (StatusCode, Json<Pagination<Model>>): IntoResponse,
{
    match pagination_query {
        query if query.page == 0 => Err(PaginationError::IsNegativeOrZero.into()),
        query if query.size == 0 => Err(PaginationError::SizeTooLow.into()),
        query if query.size > 100 => Err(PaginationError::SizeTooLarge.into()),
        query => {
            let pagination = fn_find_in_pagination(query).await?;
            Ok((StatusCode::OK, Json(pagination)))
        }
    }
}

/// Returns a controller that creates a model if it does not already exist.
///
/// # Type parameters
///
/// - `FindFunction`: The type of the function that finds the model.
/// - `FindFunctionFuture`: The type of the future returned by the function that finds the model.
/// - `CreateFunction`: The type of the function that creates the model.
/// - `CreateFunctionFuture`: The type of the future returned by the function that creates the model.
/// - `Model`: The type of the model to be created.
/// - `Error`: The type of error that can occur while creating the model.
///
/// # Arguments
///
/// - `fn_find`: A function that finds the model.
/// - `fn_create`: A function that creates the model.
/// - `error`: An error to return if the model already exists.
///
/// # Returns
///
/// A `Result` containing a `Json` object with the created model and an HTTP status code of 201 if
/// the model was created, or an error if the model already exists.
///
/// # Errors
///
/// This can fail the the create function fails.
pub async fn create_controller<
    FindFunction,
    FindFunctionFuture,
    CreateFunction,
    CreateFuntionFuture,
    Model,
    Error: std::error::Error,
>(
    fn_find: FindFunction,
    fn_create: CreateFunction,
    error: Error,
) -> Result<impl IntoResponse, Error>
where
    FindFunction: Fn() -> FindFunctionFuture,
    FindFunctionFuture: Future<Output = Result<Option<Model>, Error>>,
    CreateFunction: Fn() -> CreateFuntionFuture,
    CreateFuntionFuture: Future<Output = Result<Option<Model>, Error>>,
    (StatusCode, Json<Model>): IntoResponse,
{
    if fn_find().await?.is_some() {
        return Err(error);
    }
    let model = fn_create().await?;
    let Some(model) = model else {
        return Err(error);
    };

    Ok((StatusCode::CREATED, Json(model)))
}

/// Returns a controller that updates a model if it exists and has changed.
///
/// # Type parameters
///
/// - `FindFunction`: The type of the function that finds the model.
/// - `FindFunctionFuture`: The type of the future returned by the function that finds the model.
/// - `UpdateFunction`: The type of the function that updates the model.
/// - `UpdateFunctionFuture`: The type of the future returned by the function that updates the model.
/// - `Model`: The type of the model to be updated.
/// - `Error`: The type of error that can occur while updating the model.
///
/// # Arguments
///
/// - `fn_find`: A function that finds the model.
/// - `fn_update`: A function that updates the model.
/// - `error`: An error to return if the model does not exist.
///
/// # Returns
///
/// A `Result` containing a `Json` object with the updated model and an HTTP status code of 200 if
/// the model was updated and has changed, or an HTTP status code of 204 if the model was updated
/// but has not changed, or an error if the model does not exist.
///
/// # Errors
///
/// This can fail the the find or update function fails.
pub async fn update_controller<
    FindFunction,
    FindFunctionFuture,
    UpdateFunction,
    UpdateFuntionFuture,
    Model,
    Error: std::error::Error,
>(
    fn_find: FindFunction,
    fn_update: UpdateFunction,
    error: Error,
) -> Result<impl IntoResponse, Error>
where
    FindFunction: Fn() -> FindFunctionFuture,
    FindFunctionFuture: Future<Output = Result<Option<Model>, Error>>,
    UpdateFunction: Fn() -> UpdateFuntionFuture,
    UpdateFuntionFuture: Future<Output = Result<Option<Model>, Error>>,
    Model: PartialEq,
    (StatusCode, Json<Model>): IntoResponse,
{
    let old_model = fn_find().await?;

    if let Some(old_model) = old_model {
        let new_model = fn_update().await?;
        if Some(&old_model) == new_model.as_ref() {
            return Ok(StatusCode::NO_CONTENT.into_response());
        };

        return Ok((StatusCode::OK, Json(new_model.unwrap_or(old_model))).into_response());
    }

    Err(error)
}

/// Returns a controller that deletes a resource if it exists.
///
/// # Type parameters
///
/// - `FindFunction`: The type of the function that finds the resource.
/// - `FindFunctionFuture`: The type of the future returned by the function that finds the resource.
/// - `DeleteFunction`: The type of the function that deletes the resource.
/// - `DeleteFunctionFuture`: The type of the future returned by the function that deletes the resource.
/// - `T`: The type of the resource to be deleted.
/// - `Error`: The type of error that can occur while deleting the resource.
///
/// # Arguments
///
/// - `fn_find`: A function that finds the resource.
/// - `fn_delete`: A function that deletes the resource.
/// - `error`: An error to return if the resource does not exist.
///
/// # Returns
///
/// A `Result` containing an HTTP status code of 200 if the resource was deleted, or an error if the
/// resource does not exist.
///
/// # Errors
///
/// This can fail the the find or delete function fails.
pub async fn delete_controller<
    FindFunction,
    FindFunctionFuture,
    DeleteFunction,
    DeleteFuntionFuture,
    T,
    Error: std::error::Error,
>(
    fn_find: FindFunction,
    fn_delete: DeleteFunction,
    error: Error,
) -> Result<impl IntoResponse, Error>
where
    FindFunction: Fn() -> FindFunctionFuture,
    FindFunctionFuture: Future<Output = Result<Option<T>, Error>>,
    DeleteFunction: Fn() -> DeleteFuntionFuture,
    DeleteFuntionFuture: Future<Output = Result<(), Error>>,
    StatusCode: IntoResponse,
{
    if fn_find().await?.is_some() {
        fn_delete().await?;
        Ok(StatusCode::OK)
    } else {
        Err(error)
    }
}
