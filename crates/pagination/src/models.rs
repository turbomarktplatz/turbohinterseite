/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
use async_trait::async_trait;
use serde::{Deserialize, Deserializer, Serialize};

#[cfg(feature = "surrealdb")]
use serde::de::DeserializeOwned;
#[cfg(feature = "surrealdb")]
use surrealdb::sql::Datetime;

/// A struct that represents a pagination query for fetching data.
#[derive(Debug, Clone, Deserialize)]
#[serde(default)]
#[cfg_attr(feature = "utoipa", derive(utoipa::IntoParams, utoipa::ToSchema))]
#[cfg_attr(feature = "utoipa", into_params(style = Form, parameter_in = Query))]
pub struct PaginationQuery {
    /// The page number to start fetching data from.
    #[serde(deserialize_with = "deserialize_page")]
    #[cfg_attr(feature = "utoipa", param(example = 20, minimum = 1, default = 1))]
    #[cfg_attr(feature = "utoipa", schema(example = 20, minimum = 1, default = 1))]
    pub page: usize,

    /// The number of elements to fetch per page.
    #[serde(deserialize_with = "deserialize_size")]
    #[cfg_attr(
        feature = "utoipa",
        param(example = 100, minimum = 1, maximum = 100, default = 20)
    )]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = 100, minimum = 1, maximum = 100, default = 20)
    )]
    pub size: usize,

    /// The start date to fetch data from.
    #[cfg_attr(feature = "utoipa", param(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        param(example = "2023-03-01T19:04:10.496195+00:00")
    )]
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-01T19:04:10.496195+00:00")
    )]
    #[cfg(feature = "surrealdb")]
    pub start_date: Option<Datetime>,

    /// The start date to fetch data from.
    #[cfg_attr(feature = "utoipa", param(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        param(example = "2023-03-01T19:04:10.496195+00:00")
    )]
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-01T19:04:10.496195+00:00")
    )]
    #[cfg(not(feature = "surrealdb"))]
    pub start_date: Option<String>,

    /// The end date to fetch data until.
    #[cfg_attr(feature = "utoipa", param(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        param(example = "2023-03-06T22:07:48.686801+00:00")
    )]
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-06T22:07:48.686801+00:00")
    )]
    #[cfg(feature = "surrealdb")]
    pub end_date: Option<Datetime>,

    /// The end date to fetch data until.
    #[cfg_attr(feature = "utoipa", param(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        param(example = "2023-03-06T22:07:48.686801+00:00")
    )]
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-06T22:07:48.686801+00:00")
    )]
    #[cfg(not(feature = "surrealdb"))]
    pub end_date: Option<String>,

    /// The order which should be used.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "asc"))]
    pub order_dir: OrderDir,
}

/// Decide how the data should be ordered, Ascending or Descending
#[derive(Debug, Clone, Copy, Default, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub enum OrderDir {
    #[default]
    /// Order dir Ascending
    #[serde(rename = "asc")]
    Asc,
    /// Order dir Descending
    #[serde(rename = "desc")]
    Desc,
}

impl std::fmt::Display for OrderDir {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            OrderDir::Asc => f.write_str("ASC"),
            OrderDir::Desc => f.write_str("DESC"),
        }
    }
}

#[allow(clippy::cast_sign_loss)]
fn deserialize_page<'de, D>(deserializer: D) -> Result<usize, D::Error>
where
    D: Deserializer<'de>,
{
    let page = isize::deserialize(deserializer)?;
    if page <= 0 {
        Err(serde::de::Error::custom("page must be greater than zero"))
    } else {
        Ok(page as usize)
    }
}

#[allow(clippy::cast_sign_loss)]
fn deserialize_size<'de, D>(deserializer: D) -> Result<usize, D::Error>
where
    D: Deserializer<'de>,
{
    let size = isize::deserialize(deserializer)?;
    if size <= 0 {
        Err(serde::de::Error::custom("size must be greater than zero"))
    } else if size > 100 {
        Err(serde::de::Error::custom("size must be lower than 100"))
    } else {
        Ok(size as usize)
    }
}

impl Default for PaginationQuery {
    fn default() -> Self {
        PaginationQuery {
            page: 1,
            size: 20,
            start_date: None,
            end_date: None,
            order_dir: OrderDir::default(),
        }
    }
}

/// A generic struct that represents a paginated list of data.
#[derive(Debug, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::IntoParams, utoipa::ToSchema))]
#[cfg_attr(feature = "utoipa", into_params(style = Form, parameter_in = Query))]
pub struct Pagination<T: std::fmt::Debug> {
    /// An array of items of type `T` containing the current page of data.
    pub data: Vec<T>,
    /// The current page number.
    #[cfg_attr(feature = "utoipa", schema(example = "1"))]
    pub current_page: usize,
    /// The total number of pages available.
    #[cfg_attr(feature = "utoipa", schema(example = "100"))]
    pub total_pages: usize,
    /// The maximum number of items returned per page.
    #[cfg_attr(feature = "utoipa", schema(example = "20"))]
    pub page_size: usize,
    /// The total number of items available across all pages.
    #[cfg_attr(feature = "utoipa", schema(example = "104"))]
    pub total_data: usize,
}

impl<T: std::fmt::Debug> Pagination<T> {
    /// Converts an object of type `T` to a pagination object of type `R`.
    ///
    /// # Type parameters
    ///
    /// - `R`: The type of the resulting pagination object. Must be convertible from `T`.
    ///
    /// # Returns
    ///
    /// A new pagination object of type `R`, with the same metadata as the original object and
    /// with each item in the `data` vector converted to type `R`.
    #[must_use]
    pub fn into_type<R: std::fmt::Debug + From<T>>(self) -> Pagination<R> {
        Pagination {
            data: self.data.into_iter().map(Into::into).collect(),
            current_page: self.current_page,
            total_pages: self.total_pages,
            page_size: self.page_size,
            total_data: self.total_data,
        }
    }
}

/// A trait that allows an object to be converted into a paginated result.
#[async_trait]
pub trait IntoPagination<P: std::fmt::Debug> {
    type Error;

    /// Converts the object into a paginated result.
    async fn into_pagination(self, query: PaginationQuery) -> Result<Pagination<P>, Self::Error>;
}

/// Implements the `IntoPagination` trait for the `Paginator` struct.
#[async_trait]
#[cfg(feature = "surrealdb")]
impl<'c, C, M> IntoPagination<M> for surrealdb::method::Query<'c, C>
where
    C: surrealdb::Connection,
    M: serde::de::DeserializeOwned + std::fmt::Debug,
{
    type Error = surrealdb::Error;

    /// Converts the `Paginator` into a paginated result.
    async fn into_pagination(self, query: PaginationQuery) -> Result<Pagination<M>, Self::Error> {
        #[derive(Default, Deserialize, Serialize)]
        struct CountData {
            count: usize,
        }

        let mut response = self
            .bind(("limit", query.size))
            .bind(("start", (query.page - 1) * query.size))
            .await?;

        let data = response.take(0)?;
        let total_data = response
            .take::<Option<CountData>>(1)?
            .unwrap_or_default()
            .count;
        let total_pages = total_data / query.size;

        Ok(Pagination {
            data,
            current_page: query.page,
            total_pages,
            page_size: query.size,
            total_data,
        })
    }
}

#[cfg(feature = "surrealdb")]
#[derive(Default, Deserialize)]
struct CountData {
    count: usize,
}

#[cfg(feature = "surrealdb")]
impl<T> TryInto<Pagination<T>> for (PaginationQuery, surrealdb::Response)
where
    T: std::fmt::Debug + DeserializeOwned,
{
    type Error = surrealdb::Error;

    fn try_into(self) -> Result<Pagination<T>, Self::Error> {
        let pagination = self.0;
        let mut response = self.1;
        let data = response.take(0)?;
        let total_data = response
            .take::<Option<CountData>>(1)?
            .unwrap_or_default()
            .count;
        let total_pages = total_data / pagination.size;

        let pagination = Pagination {
            data,
            current_page: pagination.page,
            total_pages,
            page_size: pagination.size,
            total_data,
        };

        Ok(pagination)
    }
}
