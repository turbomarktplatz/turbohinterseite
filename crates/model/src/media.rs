/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use serde::{Deserialize, Serialize};

#[cfg(feature = "surrealdb")]
use surrealdb::sql::Datetime;

#[cfg(feature = "surrealdb")]
use crate::surrealdb_types::{Id, Table};

#[cfg(feature = "surrealdb")]
pub type MediaId = Id<Media, String>;

#[cfg(not(feature = "surrealdb"))]
pub type MediaId = String;

#[cfg(feature = "surrealdb")]
impl Table for Media {
    const TABLE: &'static str = "media";
}

/// Represents a media.
#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct Media {
    /// The unique ID of the media.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "0jhh4zqbkq9bq1qdclxy"))]
    pub id: MediaId,

    /// The url of the media.
    #[cfg_attr(feature = "utoipa", schema(example = "https://picsum.photos/200/300"))]
    pub url: String,

    /// The alternative text of the media.
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "Ferris Keychain on a white paper")
    )]
    pub alt: Option<String>,

    /// The date and time when the media was created.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-01T19:04:10.496195+00:00")
    )]
    #[cfg(feature = "surrealdb")]
    pub created: Datetime,

    /// The date and time when the media was created.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-01T19:04:10.496195+00:00")
    )]
    #[cfg(not(feature = "surrealdb"))]
    pub created: String,

    /// The date and time when the media was last updated.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-06T22:07:48.686801+00:00")
    )]
    #[cfg(feature = "surrealdb")]
    pub updated: Datetime,

    /// The date and time when the media was last updated.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-06T22:07:48.686801+00:00")
    )]
    #[cfg(not(feature = "surrealdb"))]
    pub updated: String,
}

/// Represents a new media that can be created.
#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[cfg_attr(feature = "validate", derive(validator::Validate))]
pub struct NewMedia {
    /// The url of the new media.
    #[cfg_attr(feature = "utoipa", schema(example = "https://picsum.photos/200/300"))]
    #[cfg_attr(feature = "validate", validate(url))]
    pub url: String,

    /// The alternative text of the new media.
    #[cfg_attr(feature = "utoipa", schema(example = "Ferris Keychain"))]
    #[cfg_attr(feature = "validate", validate(length(min = 1, max = 30)))]
    pub alt: Option<String>,
}

/// Represents an update to a media.
#[derive(Debug, Default, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[cfg_attr(feature = "validate", derive(validator::Validate))]
pub struct UpdateMedia {
    /// The new url for the media.
    #[cfg_attr(feature = "utoipa", schema(example = "https://picsum.photos/200/300"))]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[cfg_attr(feature = "validate", validate(url))]
    pub url: Option<String>,

    /// The new alternative text for the media.
    #[cfg_attr(feature = "utoipa", schema(example = "Ferris Keychain"))]
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::double_option"
    )]
    #[cfg_attr(feature = "validate", validate(length(min = 1, max = 30)))]
    pub alt: Option<Option<String>>,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[serde(untagged)]
pub enum NewOrRelateMedia {
    New(NewMedia),
    Relate(MediaId),
}
