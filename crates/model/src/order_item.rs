/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
use serde::{Deserialize, Serialize};

#[cfg(feature = "surrealdb")]
use crate::surrealdb_types::{Id, Table};
use crate::ProductId;

#[cfg(feature = "surrealdb")]
pub type OrderItemId = Id<OrderItem, i64>;

#[cfg(not(feature = "surrealdb"))]
pub type OrderItemId = i64;

#[cfg(feature = "surrealdb")]
impl Table for OrderItem {
    const TABLE: &'static str = "orderItem";
}

/// Represents a order item.
#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct OrderItem {
    /// The unique ID of the Product Variant.
    #[serde(alias = "out")]
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "0jhh4zqbkq9bq1qdclxy"))]
    pub product_id: ProductId,

    /// The quantity of the item.
    #[cfg_attr(feature = "utoipa", schema(example = "10"))]
    pub quantity: i32,

    /// The name of the item.
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "Sticker [Size; Small, Finish: Glossy]")
    )]
    pub product_name: String,

    /// The price of of the item, the amount is given in cents.
    #[cfg_attr(feature = "utoipa", schema(example = "499"))]
    pub price: i32,

    #[cfg_attr(feature = "utoipa", schema(example = "499"))]
    #[serde(default)]
    pub amount_discount: i32,

    #[cfg_attr(feature = "utoipa", schema(example = "499"))]
    #[serde(default)]
    pub amount_tax: i32,

    #[cfg_attr(feature = "utoipa", schema(example = "499"))]
    #[serde(default)]
    pub amount_total: i32,
}

/// Represents a new order item to be created in the system.
#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[cfg_attr(feature = "validate", derive(validator::Validate))]
pub struct NewOrderItem {
    /// The unique ID of the Product Variant.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "0jhh4zqbkq9bq1qdclxy"))]
    pub product: ProductId,

    /// The quantity of the item.
    #[cfg_attr(feature = "utoipa", schema(example = "10"))]
    #[cfg_attr(feature = "validate", validate(range(min = 0, max = 10)))]
    pub quantity: i32,
}
