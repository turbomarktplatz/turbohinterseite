/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
use serde::{Deserialize, Serialize};

/// Represents the status of an order.
#[derive(Debug, Default, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[serde(rename_all = "snake_case")]
pub enum OrderStatus {
    /// The order has been placed but has not yet been processed.
    #[default]
    Pending,

    /// The order is currently being processed, but has not yet shipped.
    Processing,

    /// The order has been shipped and is en route to the recipient.
    Shipped,

    /// The order has been delivered to the recipient.
    Delivered,

    /// The order has been canceled and will not be fulfilled.
    Canceled,
}

impl OrderStatus {
    pub const VALUES: [Self; 5] = [
        Self::Pending,
        Self::Processing,
        Self::Shipped,
        Self::Delivered,
        Self::Canceled,
    ];
}

impl std::iter::Iterator for OrderStatus {
    type Item = OrderStatus;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            OrderStatus::Pending => Some(Self::Processing),
            OrderStatus::Processing => Some(Self::Shipped),
            OrderStatus::Shipped => Some(Self::Delivered),
            OrderStatus::Delivered | OrderStatus::Canceled => None,
        }
    }
}

impl std::fmt::Display for OrderStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Pending => f.write_str("Pending"),
            Self::Processing => f.write_str("Processing"),
            Self::Shipped => f.write_str("Shipped"),
            Self::Delivered => f.write_str("Delivered"),
            Self::Canceled => f.write_str("Canceled"),
        }
    }
}
