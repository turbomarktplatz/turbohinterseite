/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
use serde::{Deserialize, Serialize};

// #[cfg(feature = "surrealdb")]
// use surrealdb::sql::Datetime;

#[cfg(feature = "surrealdb")]
use crate::surrealdb_types::{Id, Table};

#[cfg(feature = "surrealdb")]
pub type CustomerId = Id<Customer, String>;

#[cfg(not(feature = "surrealdb"))]
pub type CustomerId = String;

#[cfg(feature = "stripe")]
pub type StripeId = stripe::CustomerId;

#[cfg(not(feature = "stripe"))]
pub type StripeId = String;

#[cfg(feature = "surrealdb")]
impl Table for Customer {
    const TABLE: &'static str = "customer";
}

/// Represents a customer.
#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct Customer {
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    pub id: CustomerId,

    pub stripe_id: Option<StripeId>,

    pub email: String,

    pub first_name: String,

    pub last_name: String,
}
