/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use serde::{Deserialize, Serialize};

#[cfg(feature = "surrealdb")]
use crate::surrealdb_types::{Id, Table};

#[cfg(feature = "surrealdb")]
pub type ProviderId = Id<Provider, String>;

#[cfg(not(feature = "surrealdb"))]
pub type ProviderId = String;

#[cfg(feature = "surrealdb")]
impl Table for Provider {
    const TABLE: &'static str = "provider";
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct Provider {
    pub id: ProviderId,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct NewProvider {
    pub id: ProviderId,
}
