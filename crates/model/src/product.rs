/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use std::collections::BTreeMap;

use serde::{Deserialize, Serialize};

#[cfg(feature = "surrealdb")]
use surrealdb::sql::Datetime;

use crate::{
    Attribute, AttributeOptionId, CategoryId, CategoryRecursive, Media, MediaId,
    NewOrRelateAttribute, NewOrRelateAttributeOption, NewOrRelateMedia,
};

#[cfg(feature = "surrealdb")]
use crate::surrealdb_types::{Id, Table};

#[cfg(feature = "surrealdb")]
pub type ProductId = Id<Product, String>;

#[cfg(not(feature = "surrealdb"))]
pub type ProductId = String;

#[cfg(feature = "surrealdb")]
impl Table for Product {
    const TABLE: &'static str = "product";
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct ProductSitemap {
    /// The unique ID of the product.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "e67abdf9b7bb5f1ceb4b"))]
    pub id: ProductId,

    /// The name of the product.
    #[cfg_attr(feature = "utoipa", schema(example = "Ferris Keychain"))]
    pub name: String,
}

/// Represents a product.
#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct Product {
    /// The unique ID of the product.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "e67abdf9b7bb5f1ceb4b"))]
    pub id: ProductId,

    /// The unique ID of the category.
    pub category: Option<CategoryRecursive>,

    /// The name of the product.
    #[cfg_attr(feature = "utoipa", schema(example = "Ferris Keychain"))]
    pub name: String,

    /// A description of the product.
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "Some fancy and cool Ferris keychain.")
    )]
    pub description: Option<String>,

    pub preview_media: Option<Media>,

    #[serde(skip_serializing_if = "Vec::is_empty", default)]
    pub medias: Vec<Media>,

    #[serde(skip_serializing_if = "Vec::is_empty", default)]
    pub attributes: Vec<Attribute>,

    #[serde(skip_serializing_if = "Vec::is_empty", default)]
    pub associations: Vec<Association>,

    #[cfg_attr(feature = "utoipa", schema(example = json!({"weight": "10.40 kg"})))]
    pub metadata: BTreeMap<String, String>,

    /// The price of the product in cents.
    #[cfg_attr(feature = "utoipa", schema(example = "1337"))]
    pub price: Option<i32>,

    #[cfg_attr(feature = "utoipa", schema(example = "1337"))]
    pub is_orderable: bool,

    /// The date and time when the product was created.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-01T19:04:10.496195+00:00")
    )]
    #[cfg(feature = "surrealdb")]
    pub created: Datetime,

    /// The date and time when the product was created.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-01T19:04:10.496195+00:00")
    )]
    #[cfg(not(feature = "surrealdb"))]
    pub created: String,

    /// The date and time when the product was last updated.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-06T22:07:48.686801+00:00")
    )]
    #[cfg(feature = "surrealdb")]
    pub updated: Datetime,

    /// The date and time when the product was last updated.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-06T22:07:48.686801+00:00")
    )]
    #[cfg(not(feature = "surrealdb"))]
    pub updated: String,
}

/// Represents a new product that can be created.
#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[cfg_attr(feature = "validate", derive(validator::Validate))]
pub struct NewProduct {
    /// The unique ID of the product.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "e67abdf9b7bb5f1ceb4b"))]
    pub id: ProductId,

    /// The unique ID of the new category.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "0jhh4zqbkq9bq1qdclxy"))]
    pub category: Option<CategoryId>,

    /// The name of the new product.
    #[cfg_attr(feature = "utoipa", schema(example = "Ferris Keychain"))]
    #[cfg_attr(feature = "validate", validate(length(min = 1, max = 100)))]
    pub name: String,

    /// the description of the new product.
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "Some fancy and cool Ferris keychain.")
    )]
    #[cfg_attr(feature = "validate", validate(length(min = 1, max = 100_000)))]
    pub description: Option<String>,

    /// The preview image of the new product.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "g3aankd24wy6wzw3vnsx"))]
    pub preview_media: Option<NewOrRelateMedia>,

    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(
        example = json!(["g3aankd24wy6wzw3vnsx", "jjccsem76oahslcy63go", "louwjorvv8qnw6s5ikzy"])
    ))]
    pub medias: Vec<NewOrRelateMedia>,

    pub specify: Vec<Specify>,

    #[cfg_attr(feature = "utoipa", schema(example = json!({"weight": "10.40 kg"})))]
    #[cfg_attr(feature = "validate", validate(length(min = 1, max = 100)))]
    pub metadata: BTreeMap<String, String>,
}

/// Represents an update to a product.
#[derive(Debug, Default, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[cfg_attr(feature = "validate", derive(validator::Validate))]
pub struct UpdateProduct {
    /// The new unique ID of the category.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "0jhh4zqbkq9bq1qdclxy"))]
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::double_option"
    )]
    pub category: Option<Option<CategoryId>>,

    /// The new name for the product.
    #[cfg_attr(feature = "utoipa", schema(example = "Ferris Keychain"))]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[cfg_attr(feature = "validate", validate(length(min = 1, max = 100)))]
    pub name: Option<String>,

    /// The new description for the product.
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "Some fancy and cool Ferris keychain.")
    )]
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::double_option"
    )]
    #[cfg_attr(feature = "validate", validate(length(min = 1, max = 100_000)))]
    pub description: Option<Option<String>>,

    /// The new preview image for the product.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "g3aankd24wy6wzw3vnsx"))]
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::double_option"
    )]
    pub preview_media: Option<Option<MediaId>>,

    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(
        example = json!(["g3aankd24wy6wzw3vnsx", "jjccsem76oahslcy63go", "louwjorvv8qnw6s5ikzy"])
    ))]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub medias: Option<Vec<NewOrRelateMedia>>,

    #[cfg_attr(feature = "utoipa", schema(example = json!({"weight": "10.40 kg"})))]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[cfg_attr(feature = "validate", validate(length(min = 1, max = 100)))]
    pub metadata: Option<BTreeMap<String, String>>,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct Association {
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "0jhh4zqbkq9bq1qdclxy"))]
    pub id: ProductId,

    #[cfg_attr(feature = "utoipa", schema(value_type = Vec<String>))]
    #[cfg_attr(feature = "utoipa", schema(example = "[0jhh4zqbkq9bq1qdclxy]"))]
    pub options: Vec<AttributeOptionId>,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct Specify {
    pub attribute: NewOrRelateAttribute,

    pub option: NewOrRelateAttributeOption,
}
