/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use std::collections::BTreeMap;

use serde::{Deserialize, Serialize};

#[cfg(feature = "surrealdb")]
use surrealdb::sql::Datetime;

use crate::{Attribute, Media, ProductId};

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct MeilisearchProduct {
    pub id: ProductId,

    pub id_md5: String,

    pub name: String,

    pub description: Option<String>,

    pub categories: BTreeMap<String, String>,

    pub preview_media: Option<Media>,

    #[serde(skip_serializing_if = "Vec::is_empty", default)]
    pub medias: Vec<Media>,

    #[serde(skip_serializing_if = "Vec::is_empty", default)]
    pub attributes: Vec<Attribute>,

    pub metadata: BTreeMap<String, String>,

    pub price: Option<i32>,

    pub is_orderable: bool,

    #[cfg(feature = "surrealdb")]
    pub created: Datetime,

    #[cfg(not(feature = "surrealdb"))]
    pub created: String,

    #[cfg(feature = "surrealdb")]
    pub updated: Datetime,

    #[cfg(not(feature = "surrealdb"))]
    pub updated: String,
}
