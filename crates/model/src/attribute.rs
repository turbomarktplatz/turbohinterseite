/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use serde::{Deserialize, Serialize};

use crate::AttributeOption;

#[cfg(feature = "surrealdb")]
use crate::surrealdb_types::{Id, Table};

#[cfg(feature = "surrealdb")]
pub type AttributeId = Id<Attribute, String>;

#[cfg(not(feature = "surrealdb"))]
pub type AttributeId = String;

#[cfg(feature = "surrealdb")]
impl Table for Attribute {
    const TABLE: &'static str = "attribute";
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct Attribute {
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "o2gpfi22noj2r740zfoe"))]
    pub id: AttributeId,

    #[cfg_attr(feature = "utoipa", schema(example = "Size"))]
    pub name: String,

    #[serde(skip_serializing_if = "Vec::is_empty", default)]
    pub options: Vec<AttributeOption>,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[cfg_attr(feature = "validate", derive(validator::Validate))]
pub struct NewAttribute {
    #[cfg_attr(feature = "utoipa", schema(example = "Size"))]
    #[cfg_attr(feature = "validate", validate(length(min = 1, max = 30)))]
    pub name: String,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[cfg_attr(feature = "validate", derive(validator::Validate))]
pub struct UpdateAttribute {
    #[cfg_attr(feature = "utoipa", schema(example = "Size"))]
    #[cfg_attr(feature = "validate", validate(length(min = 1, max = 30)))]
    pub name: String,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[serde(untagged)]
pub enum NewOrRelateAttribute {
    New(NewAttribute),
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "o2gpfi22noj2r740zfoe"))]
    Relate(AttributeId),
}
