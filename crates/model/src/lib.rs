/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
#![allow(clippy::module_name_repetitions)]
#![allow(clippy::option_option)]

mod attribute;
mod category;
mod checkout;
mod crawler;
mod customer;
mod media;
mod media_types;
mod meilisearch;
mod option;
mod order;
mod order_item;
mod order_status;
mod product;
mod provider;

#[cfg(feature = "surrealdb")]
pub mod surrealdb_types;

pub use attribute::*;
pub use category::*;
pub use checkout::*;
pub use crawler::*;
pub use customer::*;
pub use media::*;
pub use media_types::*;
pub use meilisearch::*;
pub use option::*;
pub use order::*;
pub use order_item::*;
pub use order_status::*;
pub use product::*;
pub use provider::*;

#[cfg(feature = "surrealdb")]
pub use surrealdb_types::*;
