/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
use serde::{Deserialize, Serialize};

#[cfg(feature = "surrealdb")]
use surrealdb::sql::Datetime;

#[cfg(feature = "surrealdb")]
use crate::{Id, Table};

#[cfg(feature = "surrealdb")]
pub type CategoryId = Id<Category, String>;

#[cfg(not(feature = "surrealdb"))]
pub type CategoryId = String;

#[cfg(feature = "surrealdb")]
impl Table for Category {
    const TABLE: &'static str = "category";
}

/// Represents a category.
#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct Category {
    /// The unique ID of the category.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "6yjm6zwusfo497arp6g1"))]
    pub id: CategoryId,

    /// The unique ID of the parent category.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "1o2ailu2nbbhsn2prbq7"))]
    pub parent: Option<CategoryId>,

    /// The name of the category.
    #[cfg_attr(feature = "utoipa", schema(example = "Accessoires"))]
    pub name: String,

    /// The date and time when the category was created.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-01T19:04:10.496195+00:00")
    )]
    #[cfg(feature = "surrealdb")]
    pub created: Datetime,

    /// The date and time when the category was created.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-01T19:04:10.496195+00:00")
    )]
    #[cfg(not(feature = "surrealdb"))]
    pub created: String,

    /// The date and time when the category was last updated.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-06T22:07:48.686801+00:00")
    )]
    #[cfg(feature = "surrealdb")]
    pub updated: Datetime,

    /// The date and time when the category was last updated.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-06T22:07:48.686801+00:00")
    )]
    #[cfg(not(feature = "surrealdb"))]
    pub updated: String,
}

/// Represents a category.
#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[cfg_attr(feature = "validate", derive(validator::Validate))]
pub struct CategoryRecursive {
    /// The unique ID of the category.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "6yjm6zwusfo497arp6g1"))]
    pub id: CategoryId,

    /// The unique ID of the parent category.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "1o2ailu2nbbhsn2prbq7"))]
    pub parent: Option<Box<CategoryRecursive>>,

    /// The name of the category.
    #[cfg_attr(feature = "utoipa", schema(example = "Accessoires"))]
    #[cfg_attr(feature = "validate", validate(length(min = 1, max = 30)))]
    pub name: String,
}

/// Represents a new category that can be created.
#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[cfg_attr(feature = "validate", derive(validator::Validate))]
pub struct NewCategory {
    /// The unique ID of the new parent category.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "1o2ailu2nbbhsn2prbq7"))]
    pub parent: Option<CategoryId>,

    /// The name of the new category.
    #[cfg_attr(feature = "utoipa", schema(example = "Ferris Keychain"))]
    pub name: String,
}

/// Represents an update to a category.
#[derive(Debug, Default, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[cfg_attr(feature = "validate", derive(validator::Validate))]
pub struct UpdateCategory {
    /// The new unique ID of the parent category.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "1o2ailu2nbbhsn2prbq7"))]
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::double_option"
    )]
    pub parent: Option<Option<CategoryId>>,

    /// The new name for the category.
    #[cfg_attr(feature = "utoipa", schema(example = "Ferris Keychain"))]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[cfg_attr(feature = "validate", validate(length(min = 1, max = 30)))]
    pub name: Option<String>,
}
