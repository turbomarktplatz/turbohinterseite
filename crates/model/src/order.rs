/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use serde::{Deserialize, Serialize};

#[cfg(feature = "surrealdb")]
use surrealdb::sql::Datetime;

use crate::{order_status::OrderStatus, OrderItem};

#[cfg(feature = "surrealdb")]
use crate::surrealdb_types::{Id, Table};

#[cfg(feature = "surrealdb")]
pub type OrderId = Id<Order, String>;

#[cfg(not(feature = "surrealdb"))]
pub type OrderId = String;

#[cfg(feature = "surrealdb")]
impl Table for Order {
    const TABLE: &'static str = "order";
}

/// Represents a order.
#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct Order {
    /// The unique ID of the order.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    pub id: OrderId,

    /// The name of the recipient for the shipment, as a string.
    #[cfg_attr(feature = "utoipa", schema(example = "Rusty Ferris"))]
    pub shipping_name: String,

    /// The first line of the recipient's shipping address, as a string.
    #[cfg_attr(feature = "utoipa", schema(example = "Rusty Way 1234"))]
    pub shipping_line1: String,

    /// An optional second line of the recipient's shipping address, such as an apartment or suite number, as a string.
    #[cfg_attr(feature = "utoipa", schema(example = "Apt. 5A"))]
    pub shipping_line2: Option<String>,

    /// The city of the recipient's shipping address, as a string.
    #[cfg_attr(feature = "utoipa", schema(example = "Ferrisville"))]
    pub shipping_city: String,

    /// The state or province of the recipient's shipping address, as a string.
    #[cfg_attr(feature = "utoipa", schema(example = "RUST"))]
    pub shipping_state: Option<String>,

    /// The postal code of the recipient's shipping address, as a string.
    #[cfg_attr(feature = "utoipa", schema(example = "12345"))]
    pub shipping_zip_code: String,

    /// The country of the recipient's shipping address, as a string.
    pub shipping_country: String,

    /// The list of items that should be included in the order, specified as UUIDs.
    pub items: Vec<OrderItem>,

    /// The status of the order, indicating whether it has been shipped, delivered, etc.
    pub status: OrderStatus,

    /// The price amount of the order, including all items, taxes, and shipping fees in cents.
    #[cfg_attr(feature = "utoipa", schema(example = "1337"))]
    pub amount_total: i32,

    /// The discount amount of the order, in cents.
    #[cfg_attr(feature = "utoipa", schema(example = "300"))]
    pub amount_discount: i32,

    /// The tax amount for the order, in cents.
    #[cfg_attr(feature = "utoipa", schema(example = "166"))]
    pub amount_tax: i32,

    /// The shipping fee for the order, in cents.
    #[cfg_attr(feature = "utoipa", schema(example = "499"))]
    pub shipping_fee: i32,

    /// The date and time when the order was created.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-01T19:04:10.496195+00:00")
    )]
    #[cfg(feature = "surrealdb")]
    pub created: Datetime,

    /// The date and time when the order was created.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-01T19:04:10.496195+00:00")
    )]
    #[cfg(not(feature = "surrealdb"))]
    pub created: String,

    /// The date and time when the order was last updated.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-06T22:07:48.686801+00:00")
    )]
    #[cfg(feature = "surrealdb")]
    pub updated: Datetime,

    /// The date and time when the order was last updated.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-06T22:07:48.686801+00:00")
    )]
    #[cfg(not(feature = "surrealdb"))]
    pub updated: String,
}

/// Represents an update to a product.
#[derive(Debug, Default, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct UpdateOrder {
    /// The status of the order, indicating whether it has been shipped, delivered, etc.
    pub status: OrderStatus,
}
