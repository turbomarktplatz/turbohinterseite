/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use serde::{Deserialize, Serialize};

use crate::{ProductId, ProviderId};

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[cfg_attr(feature = "validate", derive(validator::Validate))]
pub struct NewMarketPrice {
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "0jhh4zqbkq9bq1qdclxy"))]
    pub provider: ProviderId,

    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "0jhh4zqbkq9bq1qdclxy"))]
    pub product: ProductId,

    #[cfg_attr(feature = "utoipa", schema(example = "1337"))]
    #[cfg_attr(feature = "validate", validate(range(min = 0, max = 9_999_99)))]
    pub price: i32,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[cfg_attr(feature = "validate", derive(validator::Validate))]
pub struct NewSupplierPrice {
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "0jhh4zqbkq9bq1qdclxy"))]
    pub provider: ProviderId,

    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "0jhh4zqbkq9bq1qdclxy"))]
    pub product: ProductId,

    #[cfg_attr(feature = "utoipa", schema(example = "1337"))]
    #[cfg_attr(feature = "validate", validate(range(min = 0, max = 9_999_99)))]
    pub suggest_retail_price: i32,

    #[cfg_attr(feature = "utoipa", schema(example = "1337"))]
    #[cfg_attr(feature = "validate", validate(range(min = 0, max = 9_999_99)))]
    pub purchasing_price: i32,
}
