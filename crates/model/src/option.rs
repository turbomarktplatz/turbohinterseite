/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
use serde::{Deserialize, Serialize};

#[cfg(feature = "surrealdb")]
use crate::surrealdb_types::{Id, Table};

#[cfg(feature = "surrealdb")]
pub type AttributeOptionId = Id<AttributeOption, String>;

#[cfg(not(feature = "surrealdb"))]
pub type AttributeOptionId = String;

#[cfg(feature = "surrealdb")]
impl Table for AttributeOption {
    const TABLE: &'static str = "option";
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct AttributeOption {
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "2vyo0b9ncfsal6ub87we"))]
    pub id: AttributeOptionId,

    #[cfg_attr(feature = "utoipa", schema(example = "Small"))]
    pub name: String,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[cfg_attr(feature = "validate", derive(validator::Validate))]
pub struct NewAttributeOption {
    #[cfg_attr(feature = "utoipa", schema(example = "Small"))]
    #[cfg_attr(feature = "validate", validate(length(min = 1, max = 30)))]
    pub name: String,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[cfg_attr(feature = "validate", derive(validator::Validate))]
pub struct UpdateAttributeOption {
    #[cfg_attr(feature = "utoipa", schema(example = "Small"))]
    #[cfg_attr(feature = "validate", validate(length(min = 1, max = 30)))]
    pub name: String,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[serde(untagged)]
pub enum NewOrRelateAttributeOption {
    New(NewAttributeOption),
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "o2gpfi22noj2r740zfoe"))]
    Relate(AttributeOptionId),
}
