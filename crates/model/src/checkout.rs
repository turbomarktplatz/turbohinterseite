/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use std::collections::HashSet;

use serde::{Deserialize, Serialize};

#[cfg(feature = "surrealdb")]
use surrealdb::sql::Datetime;

use crate::{CustomerId, NewOrderItem, OrderItem};

#[cfg(feature = "surrealdb")]
use crate::surrealdb_types::{Id, Table};

#[cfg(feature = "surrealdb")]
pub type CheckoutId = Id<Checkout, String>;

#[cfg(not(feature = "surrealdb"))]
pub type CheckoutId = String;

#[cfg(feature = "surrealdb")]
impl Table for Checkout {
    const TABLE: &'static str = "checkout";
}

/// Represents a checkout validation.
#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct ValidateCheckout {
    /// The validation key which will be used to check the checkout
    pub validation_key: String,
}

/// Represents a checkout.
#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct Checkout {
    /// The unique ID of the order.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "e67abdf9b7bb5f1ceb4b"))]
    pub id: CheckoutId,

    /// The checkout url which the user should redirect to.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "https://some.checkout.url"))]
    pub checkout_url: Option<String>,

    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "f362dc8b-1244-420b-92d8-c51706b5f7aa")
    )]
    pub customer: CustomerId,

    #[cfg_attr(feature = "utoipa", schema(value_type = Vec<String>))]
    #[cfg_attr(feature = "utoipa", schema(example = "[https://some.checkout.url]"))]
    pub items: HashSet<OrderItem>,

    /// The date and time when the order was created.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-01T19:04:10.496195+00:00")
    )]
    #[cfg(feature = "surrealdb")]
    pub created: Datetime,

    /// The date and time when the order was created.
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(
        feature = "utoipa",
        schema(example = "2023-03-01T19:04:10.496195+00:00")
    )]
    #[cfg(not(feature = "surrealdb"))]
    pub created: String,
}

/// Represents a checkout.
#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct CheckoutGuest {
    /// The checkout url which the user should redirect to.
    pub checkout_url: String,
}

/// Represents a new order to be created in the system for a specific customer.
#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[cfg_attr(feature = "validate", derive(validator::Validate))]
pub struct NewCustomerCheckout {
    /// The list of items that should be included in the order, specified as UUIDs.
    #[cfg_attr(feature = "utoipa", schema(value_type = Vec<String>))]
    #[cfg_attr(feature = "utoipa", schema(example = "[https://some.checkout.url]"))]
    #[cfg_attr(feature = "validate", validate(length(min = 1)))]
    pub items: HashSet<NewOrderItem>,
}

/// Represents a new order to be created in the system.
#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[cfg_attr(feature = "validate", derive(validator::Validate))]
pub struct NewCheckout {
    /// The list of items that should be included in the order, specified as UUIDs.
    #[cfg_attr(feature = "utoipa", schema(value_type = Vec<String>))]
    #[cfg_attr(feature = "utoipa", schema(example = "[https://some.checkout.url]"))]
    #[cfg_attr(feature = "validate", validate(length(min = 1)))]
    pub items: HashSet<NewOrderItem>,
}
