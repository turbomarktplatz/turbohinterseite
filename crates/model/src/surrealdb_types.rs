/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use std::{hash::Hash, marker::PhantomData};

use serde::{de::DeserializeOwned, Deserialize, Deserializer, Serialize};
use surrealdb::sql::Thing;

pub trait Table {
    const TABLE: &'static str;
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
pub struct PhantomId<T, ID>
where
    T: Table,
    ID: PartialEq + Eq + Hash + DeserializeOwned + Serialize + Into<surrealdb::sql::Id>,
{
    pub _type: PhantomData<T>,
    pub inner: ID,
}

#[derive(Debug, Clone, Deserialize)]
#[cfg_attr(feature = "utoipa", derive(utoipa::ToSchema))]
#[serde(untagged)]
pub enum Id<T, ID>
where
    T: Table,
    ID: PartialEq + Eq + Hash + DeserializeOwned + Serialize + Into<surrealdb::sql::Id>,
{
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "0jhh4zqbkq9bq1qdclxy"))]
    Surreal(Thing),

    #[serde(deserialize_with = "deserialize_phantom_id")]
    #[cfg_attr(feature = "utoipa", schema(value_type = String))]
    #[cfg_attr(feature = "utoipa", schema(example = "0jhh4zqbkq9bq1qdclxy"))]
    PhantomId(PhantomId<T, ID>),
}

impl<T, ID> PartialEq for Id<T, ID>
where
    T: Table,
    ID: ToString + PartialEq + Eq + Hash + DeserializeOwned + Serialize + Into<surrealdb::sql::Id>,
{
    fn eq(&self, other: &Self) -> bool {
        self.to_string().eq(&other.to_string())
    }
}

impl<T, ID> Eq for Id<T, ID>
where
    T: Table,
    ID: ToString + PartialEq + Eq + Hash + DeserializeOwned + Serialize + Into<surrealdb::sql::Id>,
{
}

impl<T, ID> Hash for Id<T, ID>
where
    T: Table,
    ID: ToString + PartialEq + Eq + Hash + DeserializeOwned + Serialize + Into<surrealdb::sql::Id>,
{
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        Hash::hash(&self.to_string(), state)
    }
}

impl<T, ID> Id<T, ID>
where
    T: Table,
    ID: PartialEq + Eq + Hash + DeserializeOwned + Serialize + Into<surrealdb::sql::Id>,
{
    pub fn new(id: ID) -> Self {
        Self::PhantomId(PhantomId {
            _type: PhantomData::default(),
            inner: id,
        })
    }
}

impl<T, ID> ToString for Id<T, ID>
where
    T: Table,
    ID: ToString + PartialEq + Eq + Hash + DeserializeOwned + Serialize + Into<surrealdb::sql::Id>,
{
    fn to_string(&self) -> String {
        match self {
            Id::Surreal(thing) => thing.id.to_raw(),
            Id::PhantomId(phantom_id) => phantom_id.inner.to_string(),
        }
    }
}

fn deserialize_phantom_id<
    'de,
    D,
    T: Table,
    ID: PartialEq + Eq + Hash + DeserializeOwned + Serialize + Into<surrealdb::sql::Id>,
>(
    deserializer: D,
) -> Result<PhantomId<T, ID>, D::Error>
where
    D: Deserializer<'de>,
{
    ID::deserialize(deserializer).map(|inner| PhantomId {
        _type: PhantomData::<T>::default(),
        inner,
    })
}

impl<T, ID> Into<Thing> for Id<T, ID>
where
    T: Table,
    ID: PartialEq + Eq + Hash + DeserializeOwned + Serialize + Into<surrealdb::sql::Id>,
{
    fn into(self) -> Thing {
        match self {
            Id::Surreal(thing) => thing,
            Id::PhantomId(data) => Thing {
                tb: T::TABLE.to_owned(),
                id: data.inner.into(),
            },
        }
    }
}

impl<T, ID> Serialize for Id<T, ID>
where
    T: Table,
    ID: PartialEq + Eq + Hash + DeserializeOwned + Serialize + Into<surrealdb::sql::Id> + Clone,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        match self {
            Id::Surreal(thing) => match &thing.id {
                surrealdb::sql::Id::Number(value) => value.serialize(serializer),
                surrealdb::sql::Id::String(value) => value.serialize(serializer),
                surrealdb::sql::Id::Array(value) => value.serialize(serializer),
                surrealdb::sql::Id::Object(value) => value.serialize(serializer),
                surrealdb::sql::Id::Generate(value) => value.serialize(serializer),
            },
            Id::PhantomId(data) => Thing {
                tb: T::TABLE.to_owned(),
                id: data.inner.clone().into(),
            }
            .serialize(serializer),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::{
        hash::{DefaultHasher, Hash, Hasher},
        marker::PhantomData,
    };

    use surrealdb::sql::Thing;

    use crate::{Id, PhantomId, Table};

    #[derive(Debug, Hash)]
    struct TestTable;

    impl Table for TestTable {
        const TABLE: &'static str = "table";
    }

    #[test]
    fn test_partial_eq() {
        let this = Id::<TestTable, String>::Surreal(Thing::from(("table", "b")));
        let other = Id::<TestTable, String>::PhantomId(PhantomId {
            _type: PhantomData::default(),
            inner: String::from("b"),
        });

        assert_eq!(this, other);
    }

    #[test]
    fn test_hash() {
        let this = Id::<TestTable, String>::Surreal(Thing::from(("table", "b")));
        let other = Id::<TestTable, String>::PhantomId(PhantomId {
            _type: PhantomData::default(),
            inner: String::from("b"),
        });

        fn calculate_hash<T: Hash>(t: &T) -> u64 {
            let mut s = DefaultHasher::new();
            t.hash(&mut s);
            s.finish()
        }

        assert_eq!(calculate_hash(&this), calculate_hash(&other));
    }
}
